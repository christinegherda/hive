import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/main.css';
import '../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
import Routes from './Routes';

ReactDOM.render(<Routes />, document.getElementById('root'));