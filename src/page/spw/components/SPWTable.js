import React from "react";
import {BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

export default class SPWTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.siteUrlFormatter = this.siteUrlFormatter.bind(this);
  }
  siteUrlFormatter(cell, detail) {
    return (
      <a href={cell} className="btn btn-primary btn-xs" target="_blank"><i className="fa fa-link"></i> Visit Site</a>
    );
  }
  render() {
    const spw = this.props.spw;
    // console.log(spw);
    // console.log(typeof spw);
    // spw.map(function(spwListing) {
    //   console.log(spwListing);
    // });
    return (
      <BootstrapTable data={ spw }>
        <TableHeaderColumn dataField='ID' isKey>ID</TableHeaderColumn>
        <TableHeaderColumn dataField='ListingID'>Listing ID</TableHeaderColumn>
        <TableHeaderColumn dataField='address'>Address</TableHeaderColumn>
        <TableHeaderColumn dataField='broker'>Broker</TableHeaderColumn>
        <TableHeaderColumn dataField='site_url' dataFormat={this.siteUrlFormatter} >Site URL</TableHeaderColumn>
      </BootstrapTable>
    );
  }
}
