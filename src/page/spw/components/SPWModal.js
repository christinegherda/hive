import React from "react";
import { Link } from "react-router-dom";
import {BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Modal, Tooltip, OverlayTrigger} from 'react-bootstrap';
import SPWTable from './SPWTable.js';
import $ from 'jquery';
import axios from 'axios';

const hiveConfig = require('../../../hive.config.js');
const auth = require('../../../session/auth.js');

export default class SPWModal extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false,
      fetching: true,
      spw: []
    }
    this.handleClose = this.handleClose.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.displaySPWResults = this.displaySPWResults.bind(this);
    this.siteUrlFormatter = this.siteUrlFormatter.bind(this);

  }

  handleClose() {
    this.setState({ show: false });
  }
  handleOpen() {

    const spw = this.props.spw;
    const agent_id = spw.UserId;
    const spwComponent = this;

    spwComponent.setState({ show: true });

    axios.post(`${hiveConfig.ace.apiURL}fetch-activated-spw/`, {'agent_id' : agent_id})
    .then(result => {

      spwComponent.setState({
        fetching: false,
        spw: result.data
      });

    });

  }

  siteUrlFormatter(cell, detail) {
    return (
      <a href={cell} className="btn btn-primary btn-xs" target="_blank"><i className="fa fa-link"></i> Visit Site</a>
    );
  }

  displaySPWResults() {

    return (
      <SPWTable spw={this.state.spw}/>
    );
  }
  render() {

    const { spw } = this.state;

    return (
      <div>
        <button className="button btn-success" onClick={this.handleOpen} >
          <i className="fa fa-list-alt"></i> View SPW
        </button>
        <Modal show={this.state.show} onHide={this.handleClose} className="modal_hive modal-dashboard">
          <Modal.Header closeButton><b><i className="fa fa-user"></i> {spw.full_name}</b> : <i>Active Single Property Websites</i></Modal.Header>
          <Modal.Body>
            { this.state.fetching ?
              <div className="fetching">
                <h3 className="text-center"><i className="fa fa-refresh fa-spin"></i> Fetching Single Property Websites </h3>
              </div>
              :
              <div className="data">

                <BootstrapTable data={ spw }>
                  <TableHeaderColumn hidden dataField='ID' isKey>ID</TableHeaderColumn>
                  <TableHeaderColumn dataField='ListingID'>Listing ID</TableHeaderColumn>
                  <TableHeaderColumn dataField='address'>Address</TableHeaderColumn>
                  <TableHeaderColumn dataField='broker'>Broker</TableHeaderColumn>
                  <TableHeaderColumn width='95' dataField='site_url' dataFormat={this.siteUrlFormatter} >Site URL</TableHeaderColumn>
                </BootstrapTable>
              </div>
             }


          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
