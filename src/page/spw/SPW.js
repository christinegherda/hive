import React from "react";
import axios from 'axios';
import $ from 'jquery';
import {BootstrapTable, TableHeaderColumn, SearchField, ClearSearchButton} from 'react-bootstrap-table';
import { Modal, Tooltip, OverlayTrigger} from 'react-bootstrap';
import TopNav from '../../components/common/Topnav.js';
import Sidebar from '../../components/common/Sidebar.js';
import PageTitle from '../../components/common/PageTitle.js';
import Footer from '../../components/common/Footer.js';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import SPWModal from './components/SPWModal.js';

const title = [ 'SPW' ];
const auth = require('../../session/auth.js');
const hiveConfig = require('../../hive.config.js');
var dateFormat = require('dateformat');



export default class SPW extends React.Component {

  constructor(props) {
    super(props);

    const authStatus = auth.checkAuth().then(function(e) {
      console.log(e);
      if(e) {
        // True

      }
      else {
        // False, continue
        console.log("Un authenticated, please log in");
        window.location = "/";
      }
    });
    this.state = {
      data: null,
      loading: true,
      isDataFetched: false,
      page: 1,
      offset: 0,
      limit: 20,
      total: 0,
      search:'',
      sortItem:'',
      sortOrder:'',
      showSpw: false
    };
    this.filterClick = this.filterClick.bind(this);
    this.checkFunction = this.checkFunction.bind(this);
    this.actionButtonFormatter = this.actionButtonFormatter.bind(this);
    this.handleSpwModalClose = this.handleSpwModalClose.bind(this);
    this.handleActionDetail = this.handleActionDetail.bind(this);
  }
  filterClick(event){
    $(".filterOption").toggleClass("show");
  }
  componentWillMount() {
    let currentComponent = this;

    var params = {
      'limit': this.state.limit,
      'search': this.state.search,
      'offset': this.state.offset,
      'page': this.state.page,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
    };
    axios.all([this.getTotalSpw(params), this.getSpw(params)])
    .then(
      axios.spread(function (totalSpw, spw) {
        console.log("Total",totalSpw.data.length);
        currentComponent.setState({
          total: totalSpw.data.length,
          data: spw.data,
          isDataFetched: true,
          loading: false
        });

        $('.right_col').css('min-height', $(document).height() + 200);
        $('.left_col').css('min-height', $(document).height());

      })
    );
  }
  getSpw(params) {
    return axios.post(`${hiveConfig.ace.apiURL}fetch-spw`, params);
  }
  getTotalSpw(params) {
    return axios.post(`${hiveConfig.ace.apiURL}fetch-total-spw`, params);
  }

  actionButtonFormatter(cell, detail) {
    if(detail.is_subdomain) {
      return '';
    }
    else {
      return (
        <SPWModal spw={detail}/>
      );
    }
  }
  checkFunction(domain) {
    // domain.preventDefault();
    console.log(domain);
  }
  handleActionDetail(detail) {
    console.log(detail);
    this.setState({ showSpw: true });

  }
  handleSpwModalClose(detail) {
    console.log(detail);
    this.setState({ showSpw: false });
  }
  dateFormatter(cell, agents) {
    return agents.date_created ? dateFormat(agents.date_created, "mmmm d, yyyy") : "";
  }
  onPageChange(number, size) {

    let currentComponent = this;
    var domOffset = (size * number) - this.state.limit;

    var params = {
      'limit': this.state.limit,
      'search': this.state.search,
      'offset': domOffset,
      'page': number,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
    };

    axios.all([this.getTotalSpw(params), this.getSpw(params)])
    .then(
      axios.spread(function (totalSpw, spw) {
        currentComponent.setState({
          total: totalSpw.data.length,
          data: spw.data,
          isDataFetched: true,
          loading: false
        });
      })
    )
    .catch(function(error) {
      console.log(error);
    });
  }
  onSortChange(sortItem, sortOrder){
    const currentComponent = this;
    console.log(sortItem);
    console.log(sortOrder);
    const params = {
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : sortItem,
      'sortOrder' : sortOrder,
    };

    console.log(params);

    axios.post(`${hiveConfig.ace.apiURL}fetch-spw`, params)
    .then(function (response) {
      currentComponent.setState({
        data: response.data,
        sortItem: sortItem,
        sortOrder: sortOrder,
        loading: false
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  onSearchChange(searchText, colInfos, multiColumnSearch) {
    // Does nothing, but will be handled by trigger
  }
  createCustomSearchField (props) {
    return (
      <SearchField
        className='form-control'
        id='domainSearch'
        onKeyPress={(event) => {
          if (event.key === "Enter") {
            this.handleSearchButtonClick(event)
          }
        }}
        placeholder='Search via Email, First name or Last name'

      />
    );
  }
  createCustomClearButton(onClick) {
    return (
      <ClearSearchButton
        btnText='Search'
        btnContextual='btn-primary'
        className='btn btn-primary'
        onClick={ e => this.handleSearchButtonClick(onClick) }/>
    );
  }
  handleSearchButtonClick (onClick) {

    let currentComponent = this;

    var searchText = document.getElementById("domainSearch").value;
    console.log('Trigger Search ', searchText);

    currentComponent.setState({
      loading: true
    });

    const params = {
      'search' : searchText,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder
    };

    axios.all([this.getTotalSpw(params), this.getSpw(params)])
    .then(
      axios.spread(function (totalSpw, spw) {
        currentComponent.setState({
          total: totalSpw.data.length,
          data: spw.data,
          isDataFetched: true,
          loading: false,
          search: searchText
        });

        $('.right_col').css('min-height', $(document).height() + 200);
        $('.left_col').css('min-height', $(document).height());

      })
    )
    .catch(function(error) {
      console.log(error);
    });

  }
  onSizePerPageList(list) {

    console.log("Page List: ",list);

    let currentComponent = this;

    var params = {
      'limit': list,
      'search': this.state.search,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
    };

    axios.all([this.getTotalSpw(params), this.getSpw(params)])
    .then(
      axios.spread(function (totalSpw, spw) {
        currentComponent.setState({
          total: totalSpw.data.length,
          data: spw.data,
          isDataFetched: true,
          loading: false
        });
      })
    )
    .catch(function(error) {
      console.log(error);
    });

  }
  _setTableOption() {
    return (this.state.isDataFetched) ? 'No Data Available' : <div>{this.fetchingDataLoader()}</div>;
  }
  fetchingDataLoader() {
    let loader;
    if (this.state.loading) {
      loader =
        <div className="page-loader">
          <div className="loadertext"><i className="fa  fa-circle-o-notch fa-spin"></i> <br/> Fetching Data</div>
        </div>
    } else {
      loader = <div></div>
    }
    return (
        <div>{loader}</div>
    )
  }
  spwTypeFormatter(cell, row)  {

    return (
      <span className="label label-info">{cell}</span>
    );
  }
  sourceFormatter(cell, row)  {
    const source = cell ? 'SPARK' : 'AGENTSQUARED';
    return (
      <span className="label label-primary">{source}</span>
    );
  }
  renderTable(data) {

    const spw = data;
    console.log(this.state.total);
    const options = {
      sizePerPage: this.state.limit,
      sizePerPageList: [ 20, 40, 100, 500, 1000 ],
      onSizePerPageList: this.onSizePerPageList.bind(this),
      onPageChange: this.onPageChange.bind(this),
      onSortChange: this.onSortChange.bind(this),
      onSearchChange: this.onSearchChange.bind(this),
      searchField: this.createCustomSearchField.bind(this),
      clearSearch: true,
      clearSearchBtn: this.createCustomClearButton.bind(this),
      paginationLoop: true,
      page: this.state.page,
      searchDelayTime: 1500,
      pageStartIndex: 1,
      paginationSize: 3,
      prePage: 'Prev',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      paginationShowsTotal: true,
      //hideSizePerPage: true,
      noDataText: this._setTableOption(),
      toolBar: this.createCustomToolBar
    };
    return (
      <div>
        <BootstrapTable
            data={spw}
            striped
            hover
            width="100%"
            scrollTop={ 'Bottom' }
            className="hive_table "
            bordered={false}
            pagination={true}
            options={options}
            remote={ true }
            fetchInfo={ { dataTotalSize: this.state.total } }
            search={ true }
            >
            <TableHeaderColumn width='55'  dataSort={ false } dataField='uid' isKey>User ID</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ false } dataField='email' >Email</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ false } dataField='full_name'  >Agent</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ false } dataField='mls_name' dataFormat={this.spwTypeFormatter}>MLS</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ false } dataField='EventType'>Status</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ false } dataField='isFromSpark' dataFormat={this.sourceFormatter}>Source</TableHeaderColumn>
            <TableHeaderColumn width='100' dataSort={ false } dataField='' dataFormat={this.actionButtonFormatter} >Action</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }

  render() {
    return (
      <div className="container body">
        <div className="main_container">
          <Sidebar />
          <TopNav />
          <div className="right_col" role="main">
            <div className="col-md-12">
              <PageTitle title={title} />
            </div>
            <div className="col-md-12">
              {this.renderTable(this.state.data)}
            </div>

          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
