import React from "react";
import {Button, Modal} from 'react-bootstrap';
import axios from 'axios'
import $ from 'jquery';
import { confirmAlert } from 'react-confirm-alert';
const hiveConfig = require('../../../hive.config.js');

export default class DeleteUserModal extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      show: false
    };
    this.handleDeleteUser = this.handleDeleteUser.bind(this);
  }
  handleDeleteUser(user) {
    console.log(user);
    const params = { 'id': user.id };
    confirmAlert({
        title: 'Confirm to delete user.',
        message: 'Are you sure to do this?',
        buttons: [
        {
            label: 'Yes',
            onClick: () => {
              console.log("Yes");
              axios.post(`${hiveConfig.ace.apiURL}delete-user`, params).then(function(results) {
                console.log('User Deleted');
                window.location.reload();
              }).catch(function(error) {
                throw error;
              });
            }
        },
        {
            label: 'No'
        }]
    });
  }
  render() {
    const user = this.props.agent;
    return (
      <Button className="button btn-red" onClick={ (e) => {this.handleDeleteUser(user)} }><i className="fa fa-trash"></i> Delete</Button>
    );
  }
}
