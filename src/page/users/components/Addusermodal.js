import React from "react";
import {Button, Modal} from 'react-bootstrap';
import axios from 'axios'
import $ from 'jquery';
const hiveConfig = require('../../../hive.config.js');

export default class MoreDetail extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.submitCreateUserForm = this.submitCreateUserForm.bind(this);

    this.state = {
      show: false
    };
  }

  handleClose() {
    // Reset form
    $("#createUser .username").val(''),
    $("#createUser .password").val(''),
    $("#createUser .first_name").val(''),
    $("#createUser .last_name").val(''),
    $("#createUser .email").val(''),
    $("#createUser .phone").val(''),
    $("#createUser .role").val(''),
    // Update table
    window.location.reload();
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }
  submitCreateUserForm() {
    $("#createUser .errorMessage").html("<p class='alert alert-info'><i class='fa fa-circle-o-notch fa-spin'></i> Working</p>");
    $("#createUser .errorMessage").show("fast");
    const user = {
      username    : $("#createUser .username").val(),
      password    : $("#createUser .password").val(),
      first_name  : $("#createUser .first_name").val(),
      last_name   : $("#createUser .last_name").val(),
      email       : $("#createUser .email").val(),
      phone       : $("#createUser .phone").val(),
      role        : $("#createUser .role").val(),
    };
    console.log("User Data");
    console.log(user);
    /*
      - Check if Use Exist
    */
    const params = {
      'email' : $("#createUser .email").val()
    };
    axios.post(`${hiveConfig.ace.apiURL}check-user-exist`, params).then(function(results) {
      $("#createUser .errorMessage").hide("fast");
      console.log(results);
      if(results.data.length > 0) {
        // User Exist
        console.log("User Exist");
        $("#createUser .errorMessage").html("<p class='alert alert-warning'><i class='fa fa-exclamation-triangle'></i> User Already Exist</p>");
        $("#createUser .errorMessage").show("fast");
        // Notify
      }
      else {
        // User not exist
        console.log("User Does not exist?");
        console.log(results.data.length);
        axios.post(`${hiveConfig.ace.apiURL}add-hive-user`, user).then(function(results) {
          console.log(results);
          $("#createUser .errorMessage").html("<p class='alert alert-success'><i class='fa fa-thumbs-up'></i> User successfully created.</p>");
          $("#createUser .errorMessage").show("fast");
          // window.location.reload();
        }).catch(function(error) {
          console.log(error);
          $("#createUser .errorMessage").html("<p class='alert alert-danger'><i class='fa fa-exclamation-triangle'></i> There is a problem creating user.</p>");
          $("#createUser .errorMessage").show("fast");
        });
      }

    }).catch(function(error) {
      console.log(error);
    });


  }
  render() {

    return (
      <div>
      	<Button onClick={this.handleShow} className="button btn-blue"><i className="fa fa-user-plus"></i> Add User</Button>

        <Modal show={this.state.show} onHide={this.handleClose} className="modal_hive modal-add_user">
          <Modal.Header closeButton>
            <Modal.Title><i className="fa fa-user-plus"></i> Add User</Modal.Title>
          </Modal.Header>
          <Modal.Body id="createUser">
            <div className="errorMessage"></div>
      			<div className="form-group">
      				<input type="text" className="form-control username" placeholder="Username" name="username"/>
      			</div>
      			<div className="form-group">
      				<input type="password" className="form-control password" placeholder="Password" name="password"/>
      			</div>
      			<div className="form-group">
      				<input type="text" className="form-control first_name" placeholder="First name" name="first_name"/>
      			</div>
      			<div className="form-group">
      				<input type="text" className="form-control last_name" placeholder="Last name" name="last_name"/>
      			</div>
      			<div className="form-group">
      				<input type="text" className="form-control email" placeholder="Email" name="email"/>
      			</div>
      			<div className="form-group">
      				<input type="text" className="form-control phone" placeholder="Phone" name="phone"/>
      			</div>
      			<div className="form-group">
      				<select name="" id="" className="form-control role" name="role">
      					<option value="">Select Role</option>
                <option value="admin">Admin</option>
      					<option value="tech">Tech</option>
      					<option value="sales">Sales</option>
      				</select>
      			</div>
          </Modal.Body>
          <Modal.Footer>
            <Button className="button btn-blue" onClick={this.submitCreateUserForm}> Submit </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}
