import React from "react";
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import TopNav from '../../components/common/Topnav.js';
import Sidebar from '../../components/common/Sidebar.js';
import PageTitle from '../../components/common/PageTitle.js';
import Footer from '../../components/common/Footer.js';
import AddUserModal from './components/Addusermodal.js';
import DeleteUserModal from './components/DeleteUserModal.js';
import {Button, Modal} from 'react-bootstrap';
import axios from 'axios'
const hiveConfig = require('../../hive.config.js');
const title = [ 'Users' ];
const auth = require('../../session/auth.js');
export default class Users extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null
    };
    const authStatus = auth.checkAuth().then(function(e) {
      console.log(e);
      if(e) {
        // True

      }
      else {
        // False, continue
        console.log("Un authenticated, please log in");
        window.location = "/";
      }
    });
  }
  componentWillMount() {
    let currentComponent = this;
    axios.get(`${hiveConfig.ace.apiURL}fetch-hive-users`, {}).then(function(results) {
      currentComponent.setState({
        data: results.data
      });
    }).catch(function(error) {
      console.log(error);
    });
  }

  domainFormatter(cell, agents) {
    return '<a href="http://claudialockwood.agentsquared.com" target="_blank">claudialockwood.agentsquared.com</a>';
  }

  subDomainFormatter(cell, agents) {
    return '<a href="http:/claudialockwood.agentsquared.com" target="_blank">claudialockwood.agentsquared.com</a>';
  }

  sslFormatter(cell, agents) {
    return '<span class="label label-danger">Not Secured</span>';
  }

  statusFormatter(cell, agents) {
    return '<span class="label label-success">Completed</span>';
  }


  actionButtonFormatter(cell, agents) {
    return(
      <DeleteUserModal agent={agents}/>
    );
  }

  roleFormatter(cell, row) {
    var label = '';
    switch(cell) {
      case 'admin':
        label = 'label label-primary';
        break;
      case 'tech':
        label = 'label label-info';
        break;
        break;
      case 'sales':
        label = 'label label-success';
        break;
      default:
        label = 'label label-default';
    }
    console.log(label);
    return (
      <span className={label}>{cell}</span>
    );
  }

  renderTable() {
    let data = this.state.data;
    const options = {
      page: 1,
      sizePerPage: 100,
      pageStartIndex: 1,
      paginationSize: 3,
      prePage: 'Prev',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      paginationShowsTotal: this.renderShowsTotal,
      hideSizePerPage: true,
      noDataText: 'No Agents Available',
    };

    return (
      <div>
        <BootstrapTable
            data={data}
            striped
            hover
            width="100%"
            scrollTop={ 'Bottom' }
            className="hive_table "
            bordered={false}
            pagination={true}
            options={options}
            search={ true }
            >
            <TableHeaderColumn width='55'  dataSort={ true } dataField='id' isKey>ID</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ true } dataField='first_name' >First name</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ true } dataField='last_name' >Last name</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ true } dataField='email' dataFormat={this.emailFormatter}>Email</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ true } dataField='phone' dataFormat={this.phoneFormatter}>Phone</TableHeaderColumn>
            <TableHeaderColumn width='100' dataSort={ true } dataField='role' dataFormat={this.roleFormatter}>Role</TableHeaderColumn>
            <TableHeaderColumn width='100' dataField='product' dataFormat={this.actionButtonFormatter}>Action</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }

  render() {
    return (
      <div className="container body">
        <div className="main_container">
          <Sidebar />
          <TopNav />
          <div className="right_col" role="main">
            <div className="col-md-12">
              <PageTitle title={title} />
              <AddUserModal />
            </div>
            <div className="col-md-12">
              {this.renderTable()}
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
