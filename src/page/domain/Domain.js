import React from "react";
import {BootstrapTable, TableHeaderColumn, SearchField, ClearSearchButton} from 'react-bootstrap-table';
import axios from 'axios';
import $ from 'jquery';
import TopNav from '../../components/common/Topnav.js';
import Sidebar from '../../components/common/Sidebar.js';
import PageTitle from '../../components/common/PageTitle.js';
import Footer from '../../components/common/Footer.js';
import MoreDetailModal from './components/MoreDetailModal.js';
import ChangeDomainStatus from './components/ChangeDomainStatus.js';
import Select from 'react-select';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css

const auth = require('../../session/auth.js');

const hiveConfig = require('../../hive.config.js');

var dateFormat = require('dateformat');

const title = [ 'Domains' ];

/*
 * Update specific row state from another component
 */
function updateState(datachange) {

  const { domain_id, is_ssl, is_subdomain, subdomain_url } = datachange;
  const rows = this.state.data;

  const index = rows.findIndex((x) => x.domain_id === domain_id);
  console.log('index', index);

  if (index > -1 ) {
    rows[index].is_ssl = datachange.is_ssl;
    rows[index].is_subdomain = datachange.is_subdomain;
    rows[index].domains = datachange.subdomain_url;
    this.setState({
      data: rows
    });
  }

}

export default class Domains extends React.Component {

  constructor(props) {
    super(props);
    const authStatus = auth.checkAuth().then(function(e) {
      console.log(e);
      if(e) {
        // True
      }
      else {
        // False, continue
        console.log("Un authenticated, please log in");
        window.location = "/";
      }
    });
    this.state = {
      data: null,
      loading: true,
      isDataFetched: false,
      page: 1,
      offset: 0,
      limit: 20,
      total: 0,
      search:'',
      sortItem:'',
      sortOrder:'',
      status:null,
      type:null,
      filterDomain:[],
      filterDomainType: null,
      filterDomainCategory: null,
      filterDomainStatus: null,
      openedRow: []
    };
    this.filterClick = this.filterClick.bind(this);
    this.changeStatusFormatter = this.changeStatusFormatter.bind(this);
  }

  componentWillMount() {

    let currentComponent = this;

    var params = {
      'limit': this.state.limit,
      'search': this.state.search,
      'offset': this.state.offset,
      'page': this.state.page,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
    };
    this.getDomains(params).then(function (domains) {
      currentComponent.setState({
        data: domains.data,
        isDataFetched: true,
        loading: false
      });
      currentComponent.getTotalDomains(params).then(function (totalDomains) {
        currentComponent.setState({
          total: totalDomains.data.length
        });
      });
    });
    // axios.all([this.getTotalDomains(params), this.getDomains(params)])
    // .then(
    //   axios.spread(function (totalDomains, domains) {
    //     currentComponent.setState({
    //       total: totalDomains.data.length,
    //       data: domains.data,
    //       isDataFetched: true,
    //       loading: false
    //     });

    //     $('.right_col').css('min-height', $(document).height() + 200);
    //     $('.left_col').css('min-height', $(document).height());

    //   })
    // );

  }

  getDomains(params) {
    return axios.post(`${hiveConfig.ace.apiURL}fetch-domains`, params);
    //const search = params.search ? params.search: '';
    //return axios.get(`${hiveConfig.ace.apiURL}get-domains/${this.state.limit}/${this.state.offset}/${search}`);
  }
  getTotalDomains(params) {
    return axios.post(`${hiveConfig.ace.apiURL}fetch-total-domains`, params)
    //const search = params.search ? params.search: '';
    //return axios.get(`${hiveConfig.ace.apiURL}get-total-domains/${search}`)
  }

  domainFormatter(cell, agents) {
    return '<a href="http://'+agents.domains+'" target="_blank">'+agents.domains+'</a>';
  }

  nameFormatter(cell, agents) {
    return (
      <div>
        {/* <MoreDetailModal agent={agents}/> */}
        <b><i className="fa fa-user"></i> { agents.full_name }</b>
      </div>
    );
  }
  uidFormatter(cell, agents) {
    return (
      <button className="btn btn-primary btn-xs"><i className="fa fa-plus"></i> { agents.uid }</button>
    );
  }

  sslFormatter(cell, agents) {
    this.requestSSL = async (event) => {
      let target = event.target;
      target.className = 'fa fa-refresh fa-spin';

      this.state = {
        className: '',
        message: ''
      };

      const alertOptions = {
        customUI: ({ onClose }) => {
          return (
            <div className={this.state.className}>
              <button onClick={() => {
                  onClose()
                }} type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div>{this.state.message}</div>
            </div>
          )
        },
        childrenElement: () => <div />,
        willUnmount: () => {}
      };

      try {
        let result = await axios.post(hiveConfig.certProcUrl, {
          domain: agents.domains
        });

        this.state.className = 'alert alert-success';
        this.state.message = `Request success for domain ${agents.domains}`

        target.className = 'fa fa-lock';
      } catch (err) {
        console.error('error:', err);

        this.state.className = 'alert alert-danger';
        this.state.message = `Request failed for domain ${agents.domains}:
        ${err.message}`

        target.className = 'fa fa-unlock-alt';
      }

      confirmAlert(alertOptions);
    }

    let ssl = agents.is_ssl;
    if (ssl === 1) {
      return '<div style="font-size: 22px; color:green"><i class="fa fa-lock"></i></div>';
    } else if (!agents.is_subdomain) {
      return (
        <i onClick={this.requestSSL} className="fa fa-unlock-alt fa-lg btn btn-outline-secondary"></i>
      );
    }
  }

  statusFormatter(cell, agents) {
    // let _is_subdomain = agents.is_subdomain;
    // if(_is_subdomain) {
    //   return (
    //       <span className="label label-info">SUBDOMAIN</span>
    //   );
    // }
    // else {
    //   let _status = agents.status;
    //   var stats_id = `stats-${agents.domain_id}`;
    //   if (_status === "completed") {
    //     return (
    //       <span className="label label-success" id={stats_id}>{agents.status}</span>
    //     )
    //   } else {
    //     return (
    //       <span className="label label-warning" id={stats_id}>{agents.status}</span>
    //     )
    //   }
    // }

    let _status = agents.status;
    var stats_id = `stats-${agents.domain_id}`;
    if (_status === "completed") {
      return (
        <span className="label label-success" id={stats_id}>{agents.status}</span>
      )
    } else {
      return (
        <span className="label label-warning" id={stats_id}>{agents.status}</span>
      )
    }
    

  }

  changeStatusFormatter(cell, agents) {

    if(agents.is_subdomain) {
      return "";
    }
    else {
      return (
        <ChangeDomainStatus agent={agents} handlestatechanges={ updateState.bind(this) } />
      );
    }

  }
  dateFormatter(cell, agents) {
    return agents.date_created ? dateFormat(agents.date_created, "mmmm d, yyyy") : "";
  }

  _setTableOption() {
    return (this.state.isDataFetched) ? 'No Data Available' : <div>{this.fetchingDataLoader()}</div>;
  }
  fetchingDataLoader() {
    let loader;
    if (this.state.loading) {
      loader =
        <div className="page-loader">
          <div className="loadertext"><i className="fa  fa-circle-o-notch fa-spin"></i> <br/> Fetching Data</div>
        </div>
    } else {
      loader = <div></div>
    }
    return (
        <div>{loader}</div>
    )
  }
  onSizePerPageList(list) {

    console.log("Page List: ",list);

    let currentComponent = this;

    var params = {
      'limit': list,
      'search': this.state.search,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
      'category': this.state.filterDomainCategory,
      'status': this.state.filterDomainStatus,
      'type': this.state.filterDomainType
    };

    axios.all([this.getTotalDomains(params), this.getDomains(params)])
    .then(
      axios.spread(function (totalDomains, domains) {
        currentComponent.setState({
          total: totalDomains.data.length,
          data: domains.data,
          isDataFetched: true,
          loading: false
        });
      })
    )
    .catch(function(error) {
      console.log(error);
    });

  }

  onPageChange(number, size) {

    let currentComponent = this;
    var domOffset = (size * number) - this.state.limit;

    var params = {
      'limit': this.state.limit,
      'search': this.state.search,
      'offset': domOffset,
      'page': number,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
      'type': this.state.filterDomainCategory,
      'status': this.state.filterDomainStatus,
      'type': this.state.filterDomainType
    };

    axios.all([this.getTotalDomains(params), this.getDomains(params)])
    .then(
      axios.spread(function (totalDomains, domains) {
        currentComponent.setState({
          total: totalDomains.data.length,
          data: domains.data,
          isDataFetched: true,
          loading: false
        });
      })
    )
    .catch(function(error) {
      console.log(error);
    });

  }

  onSearchChange(searchText, colInfos, multiColumnSearch) {
    // Does nothing, but will be handled by trigger
  }
  createCustomSearchField (props) {
    return (
      <SearchField
        className='form-control'
        id='domainSearch'
        onKeyPress={(event) => {
          if (event.key === "Enter") {
            this.handleSearchButtonClick(event)
          }
        }}
        placeholder='Search for domain name, agent name, domain type, status...'

      />
    );
  }

  createCustomClearButton(onClick) {
    return (
      <ClearSearchButton
        btnText='Search'
        btnContextual='btn-primary'
        className='btn btn-primary'
        onClick={ e => this.handleSearchButtonClick(onClick) }/>
    );
  }

  handleSearchButtonClick (onClick) {

    let currentComponent = this;

    var searchText = document.getElementById("domainSearch").value;
    console.log('Trigger Search ', searchText);

    currentComponent.setState({
      loading: true
    });

    const params = {
      'search' : searchText,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder
    };

    axios.all([this.getTotalDomains(params), this.getDomains(params)])
    .then(
      axios.spread(function (totalDomains, domains) {
        currentComponent.setState({
          total: totalDomains.data.length,
          data: domains.data,
          isDataFetched: true,
          loading: false,
          search: searchText
        });

        $('.right_col').css('min-height', $(document).height() + 200);
        $('.left_col').css('min-height', $(document).height());

      })
    )
    .catch(function(error) {
      console.log(error);
    });

  }

  onSortChange(sortItem, sortOrder){
    const currentComponent = this;
    console.log(sortItem);
    console.log(sortOrder);
    const params = {
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : sortItem,
      'sortOrder' : sortOrder,
    };

    console.log(params);

    axios.post(`${hiveConfig.ace.apiURL}fetch-domains`, params)
    .then(function (response) {
      currentComponent.setState({
        data: response.data,
        sortItem: sortItem,
        sortOrder: sortOrder,
        loading: false
      });
    })
    .catch(function (error) {
      console.log(error);
    });

  }

  filterClick(event){
    $(".filterOption").toggleClass("show");
  }

  clearFilter(event){
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    currentComponent.setState({
      filterDomainType: null,
      filterDomainCategory: null,
      filterDomainStatus: null,
    });

    var params = {
      'limit': this.state.limit,
      'search': this.state.search,
      'offset': this.state.offset,
      'page': this.state.page,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
    };
    axios.all([this.getTotalDomains(params), this.getDomains(params)])
    .then(axios.spread(function (totalDomains, domains) {
      currentComponent.setState({
        total: totalDomains.data.length,
        data: domains.data,
        loading: false
      });
    }));

  }

  updateValueDomainStatus (newValue) {
    console.log(newValue.value);
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    this.setState({
      filterDomainStatus: newValue
    });

    const params = {
      'type' : this.state.filterDomainType,
      'cateogry' : this.state.filterDomainCategory,
      'status' : newValue.value
    };

    axios.all([this.getTotalDomains(params), this.getDomains(params)])
    .then(axios.spread(function (totalDomains, domains) {
      currentComponent.setState({
        total: totalDomains.data.length,
        data: domains.data,
        loading: false,
        filterDomainStatus: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));
  }

  updateValueDomainType (newValue) {
    console.log('-----');
    console.log(newValue.value);
    console.log('-----');
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    this.setState({
      filterDomainType: newValue
    });

    const params = {
      'type' : newValue.value,
      'category' : this.state.filterDomainCategory,
      'status' : this.state.filterDomainStatus
    };

    axios.all([this.getTotalDomains(params), this.getDomains(params)])
    .then(axios.spread(function (totalDomains, domains) {
      currentComponent.setState({
        total: totalDomains.data.length,
        data: domains.data,
        loading: false,
        filterDomainType: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));

  }

  updateValueDomainCategory (newValue) {

    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    this.setState({
      filterDomainCategory: newValue
    });

    const params = {
      'category' : newValue.value,
      'type' : this.state.filterDomainType,
      'status' : this.state.filterDomainStatus
    };

    axios.all([this.getTotalDomains(params), this.getDomains(params)])
    .then(axios.spread(function (totalDomains, domains) {
      currentComponent.setState({
        total: totalDomains.data.length,
        data: domains.data,
        loading: false,
        filterDomainCategory: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));

  }

  createCustomToolBar = props => {
    return (
      <div>
        <div className='col-xs-8 col-sm-4 col-md-1'>
        { props.components.btnGroup }
        </div>
        <div className='col-xs-8 col-sm-4 col-md-5'>
          <button className="button btn-blue btn-filter" onClick={this.filterClick}>
            <i className="fa fa-search"></i> Search Filter
          </button>
        </div>

        <div className='col-xs-8 col-sm-4 col-md-6'>
          { props.components.searchPanel }
        </div>

        <div className="filterOption">
          <div className='col-xs-8 col-sm-4 col-md-12'>
            <div className="col-md-1">
              <p className="filter-name">Domain Status</p>
            </div>
            <div className="col-md-3">
              <Select
                name=""
                placeholder={
                  this.state.filterDomainStatus == 'completed' ? 'Completed' : 
                  ( this.state.filterDomainStatus ==  'registered' ? 'Registered' : 
                    ( this.state.filterDomainStatus ==  'pending' ? 'Pending' : 'Domain Status' ) )
                }
                value={this.state.filterDomainStatus}
                onChange={this.updateValueDomainStatus.bind(this)}
                options={[
                  { value:'completed', label:'Completed' },
                  { value:'registered', label:'Registered' },
                  { value:'pending', label:'Pending' }
                ]}
              />
            </div>
            <div className="col-md-3">
              <Select
                name=""
                placeholder={ this.state.filterDomainCategory == 'agent_site_domain' ? 'Agent Site Domain' : (this.state.filterDomainCategory == 'single_property_website' ? 'Single Property Website' : 'Category') }
                value={this.state.filterDomainCategory}
                onChange={this.updateValueDomainCategory.bind(this)}
                options={[
                  {value:'agent_site_domain', label:'Agent Site Domain'},
                  {value:'single_property_website', label:'Single Property Website'},
                ]}
              />
            </div>
            <div className="col-md-3">
              <Select
                name=""
                placeholder={ this.state.filterDomainType === '0' ? 'Domain' : (this.state.filterDomainType === '1' ? 'Subdomain' : 'Domain Type') }
                value={this.state.filterDomainType}
                onChange={this.updateValueDomainType.bind(this)}
                options={[
                  {value:'0', label:'Domain'},
                  {value:'1', label:'Subdomain'},
                ]}
              />
            </div>
            <div className="col-md-2">
              <button className="button btn-warning btn-clear" onClick={this.clearFilter.bind(this)}><i className="fa fa-eraser"></i> Clear Filter </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
  domainType(cell, row) {
    if(cell) {
      return (
        <span className="label label-info">SUBDOMAIN</span>
      );
    }
    else {
      return (
        <span className="label label-primary">DOMAIN</span>
      );
    }
    // return cell ? "SUBDOMAIN" : "DOMAIN";
  }
  isExpandableRow(row) {
    if (row.id < 2) return true;
    else return false;
  }

  expandComponent(row) {
    /*
      Execute API Call
      - Requirement
        - row.uid
        - row.agent_id
    */


    return (
      <div className="details-info text-center"><h3><i className="fa fa-spinner fa-spin"></i> Fetching Domain Details</h3></div>
    );
  }

  expandColumnComponent({ isExpandableRow, isExpanded }) {

    let content = '';

    if (isExpandableRow) {
      content = (isExpanded ? <i className="fa fa-angle-up"></i> : <i className="fa fa-angle-down"></i>);
    } else {
      content = ' ';
    }

    return (
      <div> <button className="btn btn-primary btn-xs"><b> {content}</b> </button> </div>
    );
  }

  trClassFormat(row, rowIndex) {
    // row is the current row data
    return `domain-row domain-agent-${row.uid}`;  // return class name.
  }
  renderTable(data) {
    const domains = data;
    const options = {
      sizePerPage: this.state.limit,
      sizePerPageList: [ 20, 40, 100, 500, 1000 ],
      onSizePerPageList: this.onSizePerPageList.bind(this),
      onPageChange: this.onPageChange.bind(this),
      onSortChange: this.onSortChange.bind(this),
      onSearchChange: this.onSearchChange.bind(this),
      searchField: this.createCustomSearchField.bind(this),
      clearSearch: true,
      clearSearchBtn: this.createCustomClearButton.bind(this),
      paginationLoop: true,
      page: this.state.page,
      searchDelayTime: 1500,
      pageStartIndex: 1,
      paginationSize: 3,
      prePage: 'Prev',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      paginationShowsTotal: true,
      //hideSizePerPage: true,
      noDataText: this._setTableOption(),
      toolBar: this.createCustomToolBar,
      expandBy: 'column',
      expandBodyClass: function (row, rowIndex, isExpanding) {

        if (isExpanding) { //collapsed
          console.log("EXPAND");
          const htmlData = ``;
          const params = {
            'uid' : row.uid
          };
          axios.post(`${hiveConfig.ace.apiURL}fetch-domain-details`, params).then(function(result) {
            console.log(result.data[0]);
            const rowData = result.data[0];
            if (!rowData) {
              console.log(rowData);
              $(".detail-" + row.uid).html(`<td colspan="10"><div class="details-info text-center"><h1 class="alert alert-danger">No Subscription Found</h1></div></td>`);
            }
            else {
              var agent_photo = '<h1><i class="fa fa-user"></i></h1>';
              console.log(rowData);
              if (rowData.agent_photo) {
                agent_photo = `<img src="https://s3-us-west-2.amazonaws.com/agentsquared-assets/uploads/photo/${rowData.agent_photo}" alt="" class="agent-photo img-responsive" />`;
              }
              const total_active = rowData.total_active ? rowData.total_active : "0";
              const total_sold = rowData.total_sold ? rowData.total_sold : "0";
              const web_leads = rowData.num_leads_web ? rowData.num_leads_web : "0";
              const flex_leads = rowData.num_leads_flex ? rowData.num_leads_flex : "0";
              const paying_customer = rowData.paid_amount ? `<span class="label label-success">YES</span>` : `<span class="label label-danger">No</span>`;
              const recent_sold_listings = (rowData.recent_sold_date !== null && rowData.recent_sold_date !== '0000-00-00' && rowData.recent_sold_date !== '0000-00-00 00:00:00') ? dateFormat(rowData.recent_sold_date, "mmmm d, yyyy") : "Not Applicable";
              const recent_active_listings = (rowData.recent_active_date !== null && rowData.recent_active_date !== '0000-00-00' && rowData.recent_active_date !== '0000-00-00 00:00:00') ? dateFormat(rowData.recent_active_date, "mmmm d, yyyy") : "Not Applicable";
              const token_status = rowData.is_valid ? `<span class="label label-success">LIVE</span>` : `<span class="label label-danger">LOST</span>`;

              const htmlData = `<td colspan="10"><div class="details-info">
              <div class="row">
                <div class="col-md-4">
                  <div class="alert alert-secondary">
                    <div class="col-md-3 text-center">
                      ${agent_photo}
                    </div>
                    <div class="col-md-9">
                      <h3 class="agent-name">${rowData.full_name}</h3>
                      <a href="tel:${rowData.phone}" class="agent-phone">${rowData.phone}</a>
                      <a href="mailto:${rowData.email}" class="agent-email">${rowData.email}</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="row alert alert-info ">
                    <div class="col-md-6 text-right">
                      <h1 class="badger"><i class="fa fa-heart"></i></h1>
                    </div>
                    <div class="col-md-6 text-left">
                      <h1>${total_active}</h1>
                      <b>Active Listings</b>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="row alert alert-success ">
                    <div class="col-md-6 text-right">
                      <h1 class="badger"><i class="fa fa-strikethrough"></i></h1>
                    </div>
                    <div class="col-md-6 text-left">
                      <h1>${total_sold}</h1>
                      <b>Sold Listings</b>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="row alert alert-warning ">
                    <div class="col-md-6 text-right">
                      <h1 class="badger"><i class="fa fa-globe"></i></h1>
                    </div>
                    <div class="col-md-6 text-left">
                      <h1>${web_leads}</h1>
                      <b>Web Leads</b>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="row alert alert-danger ">
                    <div class="col-md-6 text-right">
                      <h1 class="badger"><i class="fa fa-street-view"></i></h1>
                    </div>
                    <div class="col-md-6 text-left">
                      <h1>${flex_leads}</h1>
                      <b>Flex Leads</b>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      Domain Information
                    </div>
                    <div class="panel-body">
                      <div class="col-md-6">
                        <b>Domain URL:</b>
                        <p><a target='_blank' href=${rowData.domains}>${rowData.domains}</a></p>
                      </div>
                      <div class="col-md-6">
                        <b>Subdomain URL:</b>
                        <p><a target='_blank' href=${rowData.subdomain_url}>${rowData.subdomain_url}</a></p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      Product Information
                    </div>
                    <div class="panel-body">
                      <div class="col-md-4">
                        <b>Product:</b>
                        <p>${rowData.ApplicationType}</p>
                      </div>
                      <div class="col-md-4">
                        <b>Recent Sold Listings:</b>
                        <p>${recent_sold_listings}</p>
                      </div>
                      <div class="col-md-4">
                        <b>Recent Active Listings:</b>
                        <p>${recent_active_listings}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      Payment Details
                    </div>
                    <div class="panel-body">
                      <div class="col-md-6">
                        <b>Paying Customer:</b>
                        <p>${paying_customer}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Last Payment Date in Strip:</b>
                        <p>${rowData.paid_amount ? dateFormat(rowData.last_payment_date, "mmmm d, yyyy") : "Not Applicable"}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      Agent Information
                    </div>
                    <div class="panel-body">
                      <div class="col-md-6">
                        <b>User ID:</b>
                        <p>${rowData.uid}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Agent ID:</b>
                        <p>${rowData.agent_id}</p>
                      </div>
                      <div class="col-md-6">
                        <b>MLS ID:</b>
                        <p>${rowData.mls_id}</p>
                      </div>
                      <div class="col-md-6">
                        <b>MLS Name:</b>
                        <p>${rowData.mls_name}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Company Name:</b>
                        <p>${rowData.company}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Signup Date:</b>
                        <p>${rowData.date_signed ? rowData.date_signed : ''}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Access Token:</b>
                        <p>${rowData.access_token}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Bearer Token:</b>
                        <p>${rowData.bearer_token ? rowData.bearer_token : ''}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Token Status:</b>
                        <p>${token_status}</p>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      User Subscription
                    </div>
                    <div class="panel-body">
                      <div class="col-md-6">
                        <b>Plan:</b>
                        <p>${rowData.PricingPlan}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Plan Status:</b>
                        <p>${rowData.trial ? "TRIAL" : "ACTIVE"}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Users Subscription:</b>
                        <p>${rowData.EventType.charAt(0).toUpperCase() + rowData.EventType.slice(1)}</p>
                      </div>
                      <div class="col-md-6">
                        <b>Subscribed Via:</b>
                        <p>${rowData.IsFromSpark ? "Spark" : "Agentsquared"}</p>
                      </div>
                      <div class="col-md-12">
                        <b>Join Date (Date Agent received license):</b>
                        <p>${rowData.date_signed ? dateFormat(rowData.date_signed, "mmmm d, yyyy") : ""}</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-info">
                    <div class="panel-heading">
                      Social Media Details
                    </div>
                    <div class="panel-body">
                      <div class="col-md-12">
                        <b>Facebook Link:</b>
                        <p> ${rowData.facebook ? rowData.facebook : ''}</p>
                      </div>
                      <div class="col-md-12">
                        <b>Google Business Page Link:</b>
                        <p>${rowData.googleplus ? rowData.googleplus : ''}</p>
                      </div>
                      <div class="col-md-12">
                        <b>LinkedIn Link::</b>
                        <p>${rowData.linkedin ? rowData.linkedin : ''}</p>
                      </div>
                      <div class="col-md-12">
                        <b>Twitter Link:</b>
                        <p>${rowData.twitter ? rowData.twitter : ''}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <a href="agent?filter=${rowData.uid}" class="btn btn-link">View in Agent Tab</a>
            </div></td>`;
              const detailClassContainer = $(".detail-" + result.data[0].uid).html(htmlData);
            }

          }).catch(function(error) {
            throw error;
          });
        }
        return 'detail-' + row.uid;
      },
      expandParentClass: function (row, rowIndex) {
        return 'expand-parent-detail-' + row.uid;
      }
    };

    return (
      <div className="table-data">
        {this.fetchingDataLoader()}
        <BootstrapTable
            data={domains}
            striped
            hover
            width="100%"
            scrollTop={ 'Bottom' }
            className="hive_table "
            bordered={false}
            pagination={true}
            options={options}
            search={ true }
            exportCSV={ true }
            remote={ true }
            fetchInfo={ { dataTotalSize: this.state.total } }
            csvFileName='domains.csv'
            expandableRow={() => { return true; }}
            expandComponent={this.expandComponent}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent
            }}
            trClassName={this.trClassFormat}
            >
          <TableHeaderColumn width='55' export={true} expandable={true} dataField='uid' csvHeader="User ID" isKey>User ID</TableHeaderColumn>
          <TableHeaderColumn width='200' export={true} expandable={false} dataField='domains' csvHeader="Domain" dataFormat={this.domainFormatter}>Domain</TableHeaderColumn>
          <TableHeaderColumn width='200' export={true} expandable={false} dataField='type' csvHeader="Gategory">Category</TableHeaderColumn>
          <TableHeaderColumn width='200' export={true} expandable={false} dataField='is_subdomain' dataFormat={ this.domainType } csvHeader="Type">Type</TableHeaderColumn>
          <TableHeaderColumn width='200' export={true} expandable={false} dataField='full_name' csvHeader="Agent" dataFormat={this.nameFormatter}>Name</TableHeaderColumn>
          <TableHeaderColumn width='200' export={true} expandable={false} dataField='date_created' csvHeader="Date Created" csvFormat={this.dateFormatter} dataFormat={this.dateFormatter}>Date Created</TableHeaderColumn>
          <TableHeaderColumn width='80' export={true} expandable={false} dataField='is_ssl' csvHeader="SSL Status" dataFormat={this.sslFormatter}>SSL</TableHeaderColumn>
          <TableHeaderColumn width='100' export={true} expandable={false} csvHeader="Domain Status" dataField="status" dataFormat={this.statusFormatter}>Status</TableHeaderColumn>
          <TableHeaderColumn width='150' export={false} expandable={false} dataFormat={this.changeStatusFormatter}>Action</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }

  render() {

    return (
      <div className="container body">
        <div className="main_container">
          <Sidebar />
          <TopNav />
          <div className="right_col" role="main">
            <div className="col-md-12">
              <PageTitle title={title} />
            </div>
            <div className="col-md-12">
              {this.renderTable(this.state.data)}
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
