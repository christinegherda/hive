import React from "react";
import axios from "axios";
import $ from 'jquery';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'
import Alert from 'react-s-alert';

const hiveConfig = require('../../../hive.config.js');

export default class ChangeDomainStatus extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.handleChange = this.handleChange.bind(this);
        this.handleDeleteDomain = this.handleDeleteDomain.bind(this);
    }

    handleChange(event) {

        const domain_id = event.target.getAttribute('data-id');
        const user_id = event.target.getAttribute('data-agent');
        const domain_status = event.target.value;
        const name = event.target.getAttribute('data-name');
        const domain_name = event.target.getAttribute('data-domain');
        const email = event.target.getAttribute('data-email');

        confirmAlert({
            title: 'Confirm to change domain status.',
            message: 'Are you sure to do this?',
            buttons: [
            {
                label: 'Yes',
                onClick: () => {

                    var params = {
                        'id' : domain_id,
                        'status' : domain_status,
                    };

                    axios.post(`${hiveConfig.ace.apiURL}update-domain/`, params)

                    .then((response) => {

                        if(domain_status==="completed") {

                            var email_data = {
                                recipient:email,
                                sender: {
                                    email:'admin@agentsquared.com',
                                    name:'AgentSquared Admin'
                                },
                                //bcc:'honradokarljohn@gmail.com,christine.gonx@gmail.com',
                                subs: {
                                    first_name:name,
                                    custom_domain:domain_name,
                                    redirect_uri:'https://dashboard.agentsquared.com/cron/redirect_to_pages?id='+user_id

                                },
                                template_id:'3fefb4e7-55fa-4d5b-af73-a4633b66ab11'
                            };

                            return axios.post(`${hiveConfig.ace.apiURL}send-email/`, email_data);
                        }

                    }).then((response) => {

                        var find_id=`stats-${domain_id}`;

                        if(domain_status==="completed") {
                            $(`#${find_id}`).removeClass('label label-warning');
                            $(`#${find_id}`).addClass('label label-success');
                            $(`#${find_id}`).text(domain_status);
                        } else {
                            $(`#${find_id}`).removeClass('label label-success');
                            $(`#${find_id}`).addClass('label label-warning');
                            $(`#${find_id}`).text(domain_status);
                        }
                    })
                    .catch(function (error) {
                        console.log(error.response);
                    });
                }
            },
            {
                label: 'No',
                onClick: () => {
                    window.parent.location = window.parent.location.href;
                }
            }]
        });

    }

    handleDeleteDomain() {

        const { domain_id, domains, subdomain_url } = this.props.agent;
        console.log('this props handle', this.props);

        try {

            confirmAlert({
                title: `Delete Domain`,
                message: `Are you sure to do delete ${domains}?`,
                buttons: [
                {
                    label: 'Yes',
                    onClick: () => {

                        var params = {
                            'id' : domain_id,
                            'sub_domain_url' : subdomain_url
                        };

                        axios.post(`${hiveConfig.ace.apiURL}delete-domain/`, params).
                        then(result => {
                            Alert.success('Domain successfully deleted!');
                            this.props.handlestatechanges({
                                domain_id: domain_id,
                                is_ssl: 0,
                                is_subdomain: 1,
                                subdomain_url: subdomain_url
                            });
                        })
                        .catch(err => {
                            Alert.error(err);
                        });
                    }
                },
                {
                    label: 'No',
                    onClick: () => {
                        // window.parent.location = window.parent.location.href;
                    }
                }
              ]
            });

        } catch(event) {
            throw new Error();
        }

    }

    render() {

        const domain = this.props.agent;

        return (
            <div className="row">
                <form id="changeStatusForm" method="POST" className="col-md-8">
                    <input type="hidden" name="domain_id" value={domain.uid} />
                    <select className="form-control" defaultValue={domain.status} data-id={domain.domain_id} data-agent={domain.uid} data-domain={domain.domains} data-name={domain.full_name} data-email={domain.email} onChange={this.handleChange}>
                        <option value="completed"> Completed</option>
                        <option value="registered"> Registered </option>
                        <option value="pending"> Pending </option>
                    </select>

                </form>
                <div className="col-md-4">
                  <button type="button" className="btn btn-danger btn-sm deleteDomain" onClick={ this.handleDeleteDomain }><i className="fa fa-trash"></i></button>
                </div>
            </div>
        );
    }
}
