import React from "react";
import {Button, Modal } from 'react-bootstrap';
const dateFormat = require('dateformat');

export default class MoreDetail extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.linkFormat = this.linkFormat.bind(this);
        this.tokenStatusFormat = this.tokenStatusFormat.bind(this);

        this.state = {
            show: false
        };
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    linkFormat(link) {
        return (
            <a target='_blank' href={link}>{link}</a>
        );
    }

    tokenStatusFormat(status, bearer_token) {
        
        if(status!==null && status!==0) {
            return <span className='label label-success'> LIVE </span>
        } else {
            if(bearer_token !== null) {
                return <span className='label label-success'> LIVE </span>
            } else {
                return <span className='label label-danger'>LOST</span>
            }
        }
        
    }

    render() {

        const domain = this.props.agent;

        let domain_url = this.linkFormat(domain.domains);
        let subdomain_url = this.linkFormat(domain.subdomain_url);
        let tokenstatus = this.tokenStatusFormat(domain.is_valid, domain.bearer_token);

        return (
            <div>
                <Button onClick={this.handleShow}>{domain.full_name}</Button>

                <Modal show={this.state.show} onHide={this.handleClose} className="modal_hive">
                    <Modal.Header closeButton>
                        <Modal.Title>Agent Information</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="row">
                            <div className="col-md-3">
                                <div className="row">
                                    <div className="col-md-5">
                                        <div className="agent-image">
                                            <img src={"https://s3-us-west-2.amazonaws.com/agentsquared-assets/uploads/photo/"+domain.agent_photo} alt="" className="img-circle" />
                                        </div>
                                    </div>
                                    <div className="col-md-7">
                                        <p className="agent-name">{domain.full_name}</p>
                                        <a href="tel:{domain.phone}" className="agent-phone">{domain.phone}</a>
                                        <br />
                                        <a href="mailto:{domain.email}" className="agent-email">{domain.email}</a>
                                    </div>
                                </div>
                                <div className="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="tile-stats">
                                        <div className="icon"><i className="fa fa-heart"></i>
                                        </div>
                                        <div className="count">{domain.total_active ? domain.total_active : "0" }</div>
                                        <h3>Active Listings</h3>
                                        <p><a href="">Checkout out..</a></p>
                                    </div>
                                </div>
                                <div className="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="tile-stats">
                                        <div className="icon"><i className="fa fa-strikethrough"></i>
                                        </div>
                                        <div className="count">{domain.total_sold  ? domain.total_sold : "0" }</div>
                                        <h3>Sold Listings</h3>
                                        <p><a href="">Checkout out..</a></p>
                                    </div>
                                </div>
                                <div className="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="tile-stats">
                                        <div className="icon"><i className="fa fa-globe"></i>
                                        </div>
                                        <div className="count">{domain.num_leads_web  ? domain.num_leads_web : "0" }</div>
                                        <h3>Web Leads</h3>
                                        <p><a href="">Checkout out..</a></p>
                                    </div>
                                </div>
                                <div className="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div className="tile-stats">
                                        <div className="icon"><i className="fa fa-street-view"></i>
                                        </div>
                                        <div className="count">{domain.num_leads_flex  ? domain.num_leads_flex : "0" }</div>
                                        <h3>Flex Leads</h3>
                                        <p><a href="">Checkout out..</a></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-9">
                                <div className="agent-more-info">
                                    <div className="col-md-6">
                                        <div className="x_panel">
                                            <div className="x_title">
                                                <h2>Agent Information</h2>
                                                <div className="clearfix"></div>
                                            </div>
                                            <div className="x_content">
                                                <ul>
                                                    <li>
                                                        <p>User ID: <span> {domain.uid}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Agent ID: <span> {domain.agent_id}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>MLS ID: <span> {domain.mls_id}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>MLS Name: <span> {domain.mls_name}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Company Name: <span> {domain.company}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Signup Date: <span> {domain.date_signed ? dateFormat(domain.date_signed, "mmmm d, yyyy") : ""}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Access Token: <span> {domain.access_token ? domain.access_token : ''}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Bearer Token: <span> {domain.bearer_token ? domain.bearer_token : ''}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Token Status: {tokenstatus} </p>
                                                    </li>
                                                </ul> 
                                            </div>
                                        </div>

                                        <div className="x_panel">
                                            <div className="x_title">
                                                <h2>User Subscription</h2>
                                                <div className="clearfix"></div>
                                            </div>
                                            <div className="x_content">
                                                <ul>
                                                    <li>
                                                        <p>Plan: <span> {domain.PricingPlan}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Plan Status: <span> Active</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Users Subscription: <span> Purchased</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Subscribed Via: <span> {domain.IsFromSpark ? "Spark" : "Agentsquared"}  </span></p>
                                                    </li>
                                                    <li>
                                                        <p>Join Date (Date Agent received license): <span> {domain.date_signed ? dateFormat(domain.date_signed, "mmmm d, yyyy") : ""} </span></p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 

                                        <div className="x_panel">
                                            <div className="x_title">
                                                <h2>Domain Information:</h2>
                                                <div className="clearfix"></div>
                                            </div>
                                            <div className="x_content">
                                                <ul>
                                                    <li>
                                                        <p>Domain URL: <span> {domain_url}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Subdomain URL: <span> {subdomain_url} </span></p>
                                                    </li>
                                                    <li>
                                                        <p>Domain Expiration: <span> Soon... </span></p>
                                                    </li>
                                                    <li>
                                                        <p>SSL Expiration: <span> Soon... </span></p>
                                                    </li>
                                                    <li>
                                                        <button className="btn btn-success"><i className="fa fa-lock"></i> Renew SSL</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div> 

                                    </div>
                                    <div className="col-md-6">
                                        <div className="x_panel">
                                            <div className="x_title">
                                                <h2>Payment Details</h2>
                                                <div className="clearfix"></div>
                                            </div>
                                            <div className="x_content">
                                                <ul>
                                                    <li>
                                                        <p>Paying Customer: {domain.paid_amount ?  <span className="label label-success">YES</span> :  <span className="label label-danger">No</span>}</p>
                                                    </li>
                                                    <li>
                                                        <p>Last Payment Date in Stripe: <span>   {domain.paid_amount ? dateFormat(domain.last_payment_date, "mmmm d, yyyy") : "Not Applicable"} </span></p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="x_panel">
                                            <div className="x_title">
                                                <h2>Product Information</h2>
                                                <div className="clearfix"></div>
                                            </div>
                                            <div className="x_content">
                                                <ul>
                                                    <li>
                                                        <p>Product: <span> {domain.ApplicationType}</span></p>
                                                    </li>
                                                    <li>
                                                        <p>Date of Most Recent Sold Listings: <span> { (domain.recent_sold_date!==null && domain.recent_sold_date!=='0000-00-00' && domain.recent_sold_date!=='0000-00-00 00:00:00') ? dateFormat(domain.recent_sold_date, "mmmm d, yyyy") : "Not Applicable"} </span></p>
                                                    </li>
                                                    <li>
                                                        <p>Date of Most Recent Active Listings: <span> { (domain.recent_active_date!==null && domain.recent_active_date!=='0000-00-00' && domain.recent_active_date!=='0000-00-00 00:00:00') ? dateFormat(domain.recent_active_date, "mmmm d, yyyy") : "Not Applicable"} </span></p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="x_panel">
                                            <div className="x_title">
                                                <h2>Social Media Details</h2>
                                                <div className="clearfix"></div>
                                            </div>
                                            <div className="x_content">
                                                <ul>
                                                    <li>
                                                        <p> Facebook Link: <span> <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/">https://www.facebook.com/</a> </span></p>
                                                    </li>
                                                    <li>
                                                        <p> Google Business Page Link: <span> <a target="_blank" rel="noopener noreferrer" href="https://www.google.com/">https://www.google.com/</a> </span></p>
                                                    </li>
                                                    <li>
                                                        <p> LinkedIn Link: <span> <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/">https://www.linkedin.com/</a> </span></p>
                                                    </li>
                                                    <li>
                                                        <p> Twitter Link: <span> <a target="_blank" rel="noopener noreferrer" href="https://twitter.com/">https://twitter.com/</a> </span></p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}