import React from "react";
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import TopNav from '../../components/common/Topnav.js';
import Sidebar from '../../components/common/Sidebar.js';
import PageTitle from '../../components/common/PageTitle.js';
import Footer from '../../components/common/Footer.js';
const auth = require('../../session/auth.js');

const title = [ 'Capture Leads' , 'Agents who tried to purchase from https://www.agentsquared.com/pricing/' ];

export default class Agent extends React.Component {
  constructor(props) {
    super(props);

    const authStatus = auth.checkAuth().then(function(e) {
      console.log(e);
      if(e) {
        // True

      }
      else {
        // False, continue
        console.log("Un authenticated, please log in");
        window.location = "/";
      }
    });
  }

  domainFormatter(cell, agents) {
    return '<a href="http://claudialockwood.agentsquared.com" target="_blank">claudialockwood.agentsquared.com</a>';
  }

  subDomainFormatter(cell, agents) {
    return '<a href="http:/claudialockwood.agentsquared.com" target="_blank">claudialockwood.agentsquared.com</a>';
  }

  sslFormatter(cell, agents) {
    return '<span class="label label-danger">Not Secured</span>';
  }

  statusFormatter(cell, agents) {
    return '<span class="label label-success">Completed</span>';
  }

  actionButtonFormatter(cell, agents) {
    return '<button class="button btn-red"><i class="fa fa-trash"></i> Delete</button>';
  }

  renderTable() {
    let captureLead = [];
    for (var i = 1; i < 2001; i++) {
        captureLead.push(
          {
            no: i,
            name: 'Christine Gonzales',
            email: 'christine@agentsquared.com',
            phone: '123-4569-788',
            mlsname: 'ARMLS',
            product: 'Agent IDX Website',
            plan: '15 Days Trial'
          }
        );

    }

    const captureLeads = captureLead;

    const options = {
      page: 1,
      sizePerPage: 100,
      pageStartIndex: 1,
      paginationSize: 3,
      prePage: 'Prev',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      paginationShowsTotal: this.renderShowsTotal,
      hideSizePerPage: true,
      noDataText: 'No Agents Available',
    };
    return (
      <div>
        <BootstrapTable
            data={captureLeads}
            striped
            hover
            width="100%"
            scrollTop={ 'Bottom' }
            className="hive_table "
            bordered={false}
            pagination={true}
            options={options}
            search={ true }
            >
            <TableHeaderColumn width='55'  dataSort={ true } dataField='no' isKey>No.</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ true } dataField='name' >Name</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ true } dataField='email' dataFormat={this.emailFormatter}>Email</TableHeaderColumn>
            <TableHeaderColumn width='200' dataSort={ true } dataField='phone' dataFormat={this.phoneFormatter}>Phone</TableHeaderColumn>
            <TableHeaderColumn width='100' dataSort={ true } dataField='mlsname' >MLS</TableHeaderColumn>
            <TableHeaderColumn width='100' dataSort={ true } dataField='product' >Product</TableHeaderColumn>
            <TableHeaderColumn width='100' dataSort={ true } dataField='plan' >Plan</TableHeaderColumn>
            <TableHeaderColumn width='100' dataField='product' dataFormat={this.actionButtonFormatter}>Action</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }

  render() {
    return (
      <div className="container body">
        <div className="main_container">
          <Sidebar />
          <TopNav />
          <div className="right_col" role="main">
            <div className="col-md-12">
              <PageTitle title={title} />
            </div>
            <div className="col-md-12">
              {this.renderTable()}
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
