import React, { Component } from "react";
import axios from 'axios'
const FileDownload = require('js-file-download');
const hiveConfig = require('../../hive.config.js');

export default class AgentExport extends Component {
  componentDidMount() {
    axios.request({
      url: `${hiveConfig.ace.apiURL}fetch-agents-and-download/`,
      method: 'post',
      data: {
        search: "",
      },
    }).then(result => {
      console.log(result);
      FileDownload(result.data, `Agents_${(new Date()).toUTCString().replace(',', '').replace(/ /g, '-')}.csv`);
    }).catch(ex => {
      console.log(ex);
      alert('Unable to export agents...');
    });
  }

  render() {
    return (<div>Exporting...</div>);
  }
}