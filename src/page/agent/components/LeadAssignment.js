import React, { Component, Fragment } from 'react';
import Select from 'react-select';
import axios from 'axios'
import $ from 'jquery';
import Alert from 'react-s-alert';
const hiveConfig = require('../../../hive.config.js');

class LeadAssignment extends Component {

  constructor(props) {

    super(props);

    this.state = {
      agent: null,
      sales: null,
      lead_owner: '',
      lead_note: '',
      sales_username: ''
    };

    this.handleLeadAssignment = this.handleLeadAssignment.bind(this);
    this.handleLeadOwner = this.handleLeadOwner.bind(this);
    this.handleChange = this.handleChange.bind(this);

  }

  componentWillMount() {

    this.setState({
      agent: this.props.rowData,
      lead_owner: this.props.rowData.lead_owner,
      lead_note: this.props.rowData.lead_note,
      sales_username: this.props.rowData.sales_username,
      sales: this.props.sales_agents
    });

  }

  handleLeadAssignment = (event) => {

  	event.preventDefault();

    if(this.state.lead_owner == null) {
      Alert.error(`Please specify who's the lead owner!`);
      return true;
    }

  	const params = {
  		uid: this.state.agent.uid,
  		lead_owner: this.state.lead_owner,
  		lead_note: this.state.lead_note,
      sales_username: this.state.sales_username
  	};

  	axios.post(`${hiveConfig.ace.apiURL}add-lead-owner`, params)
  	.then(result => {
      this.props.handlestatechanges({
        uid: this.state.agent.uid,
        lead_owner: this.state.lead_owner,
        lead_note: this.state.lead_note,
        sales_username: this.state.sales_username
      });
      Alert.success('Lead assignment successfully added!');
  		//window.parent.location = window.parent.location.href;
  	})
  	.catch(error => {
  		console.log(error);
  	});

  }

  handleLeadOwner = (event) => {
  	console.log('handle lead owner value: ', event.value);
    console.log('handle lead owner label: ', event.label);
  	this.setState({
  		lead_owner: event.value,
      sales_username: event.label
  	})
  }

  handleChange = (event) => {
  	console.log(event.target.value);
  	this.setState({
  		lead_note: event.target.value
  	})
  }

  render() {

    let defaultPlaceholder = this.state.agent.sales_username ? this.state.agent.sales_username : 'Lead Owner';

  	let sales = [];

  	$.each(this.state.sales, function(i, item) {
  		sales.push(
	  		{
	  			value: item.id,
	  			label: item.username
	  		}
  		);
  	});

    return (
      <Fragment>
        <div className="col-md-4">
          <div className="panel panel-info">
            <div className="panel-heading">
               Lead Assignment
            </div>
            <div className="panel-body">
              <form onSubmit={this.handleLeadAssignment}>
                <div className="form-group">
                  	<Select
	              		name="lead_owner_select"
	              		placeholder={defaultPlaceholder}
	              		value={sales.value}
	              		onChange={this.handleLeadOwner}
	              		options={sales}
	             	/>
                </div>
                <div className="form-group">
                  <label htmlFor="Notes">Notes:</label>
                  <textarea name="lead_note" defaultValue={this.state.agent.lead_note} cols="30" rows="7" className="form-control" onChange={this.handleChange}></textarea>
                </div>
                <div className="form-group pull-right">
                  <button className="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Fragment>
    );

  }

}

export default LeadAssignment;