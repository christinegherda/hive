import React, { Component, Fragment } from 'react';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';
import axios from 'axios'
import { confirmAlert } from 'react-confirm-alert';
const hiveConfig = require('../../../hive.config.js');

class FreemiumPlanFormat extends Component {

  constructor(props) {

    super(props);
    this.setToFreemium = this.setToFreemium.bind(this);
    this.getPlanName = this.getPlanName.bind(this);
  }

  setToFreemium = async (event) => {

    const row = this.props.row;

    let target = event.target;

    target.className = 'fa fa-refresh fa-spin fa-2x';

    this.state = {
      className: '',
      message: ''
    };

    const alertOptions = {
      customUI: ({ onClose }) => {
        return (
          <div className={this.state.className}>
            <button onClick={() => {
                onClose()
              }} type="button" className="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <div>{this.state.message}</div>
          </div>
        )
      },
      childrenElement: () => <div />,
      willUnmount: () => {}
    };

    try {
      let result = await axios.post(`${hiveConfig.ace.apiURL}upgrade-freemium`, {
        id: row.uid
      });

      this.state.className = 'alert alert-success';
      this.state.message = `Agent upgraded`

      target.textContent = 'Freemium';
      target.className = '';
    } catch (err) {
      console.error('error:', err);

      this.state.className = 'alert alert-danger';
      this.state.message = `Failed to upgrade agent: ${err.message}`

      target.className = 'fa fa-user-circle fa-2x';
    }

    confirmAlert(alertOptions);

  }

  getPlanName = (row) => {

    let planName = '';
    let plan_occurence = row.paid_occurence;
    let paid_amount = row.paid_amount;
    let pricingPlan = row.PricingPlan;
    let store = row.IsFromSpark ? 'Spark' : 'AS';

    let mlsListed = [
      '20070913202326493241000000',
      '20140801122353246389000000',
      '20140402194227539412000000',
      '20040823195642954262000000',
      '20150128160104800461000000',
      '20140311223451933927000000',
      '20130226165731231246000000',
      '20160622112753445171000000',
      '20171027162842062220000000',
      '20180216183507684268000000'
    ];

    let dashboardTooltip = <Tooltip placement="top" id="tooltip-top">Move to freemium</Tooltip>;

    /*
    *  Check if row is partner to AS
    */ 
    if(mlsListed.includes(row.mls_id)) {
      /* if true, check if Trial */
      if(row.freemium_plan_id == 1) {
        planName = (<span>Freemium <br/> via {store}</span>);
      } else {
        console.log(row.paid_occurence);
        planName = (<span>Premium {row.paid_occurence} <br/> via {store}</span>);
      }

    } else {

      /* if not partner, check subscription via  */
      if (row.IsFromSpark) {

         /* check if Trial */
        if(row.trial) {
          planName = (<Fragment>
            <span>Signup as TRIAL

                <OverlayTrigger placement="top" overlay={dashboardTooltip}>
                  <i type="button" onClick={this.setToFreemium} className="fa fa-arrow-circle-up fa-2x"></i>
                </OverlayTrigger>
                <br/>  via {store}</span>
            
            </Fragment>
          );
        } else {
          planName = (<span>Purchased <br/> {row.PricingPlan}<br/> via {store}</span>);
        }
      } else {

         /* check if Trial */
        if(row.trial) {
          planName = (<Fragment>
            <span>Signup as TRIAL
                <OverlayTrigger placement="top" overlay={dashboardTooltip}>
                  <i type="button" onClick={this.setToFreemium} className="fa fa-arrow-circle-up fa-2x"></i>
                </OverlayTrigger>
                <br/>  via {store}</span>
            </Fragment>
          );
        } else {
          planName = (<span>Premium {pricingPlan} <br/> via {store}</span>);
        }

      }
    }

    return ( 
      planName
    );

  }

  render() {
    return (
      <Fragment>
        {this.getPlanName(this.props.row)}
      </Fragment>
    );

  }

}

export default FreemiumPlanFormat;