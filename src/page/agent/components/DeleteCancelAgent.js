import React, { Component, Fragment } from 'react';
import axios from 'axios'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import '../../../assets/css/react-confirm-alert.css';
import $ from 'jquery';
import Alert from 'react-s-alert';
const hiveConfig = require('../../../hive.config.js');

class DeleteCancelAgent extends Component {

  constructor(props) {
    super(props);
    this.handleDeleteAgent = this.handleDeleteAgent.bind(this);
    this.handleCancelAgent = this.handleCancelAgent.bind(this);
    this.cancelButton = this.cancelButton.bind(this);
    this.processCancelSubscription = this.processCancelSubscription.bind(this);
    this.state = {
      agent: null,
      processing: false
    };
  }

  componentWillMount() {

    this.setState({
      agent: this.props.rowData
    });

  }

  /*
    Delete Agent's Data In All Tables
  */
  handleDeleteAgent = (event) => {

    event.preventDefault();
    const { agent } = this.state;

    confirmAlert({
      title: `Delete Agent`,
      message: `Are you sure to delete ${agent.full_name}?`,
      buttons: [
        {
          label: 'Yes',
          onClick: () => {
            const system_params = {
              'users': {
                'table_name': 'users',
                'table_field': 'id',
                'table_value': agent.uid,
              },
              'account_meta': {
                'table_name': 'account_meta',
                'table_field': 'user_id',
                'table_value': agent.uid,
              },
              'homepage_properties': {
                'table_name': 'homepage_properties',
                'table_field': 'user_id',
                'table_value': agent.uid,
              },
              'pricing': {
                'table_name': 'pricing',
                'table_field': 'user_id',
                'table_value': agent.uid,
              },
              'freemium': {
                'table_name': 'freemium',
                'table_field': 'subscriber_user_id',
                'table_value': agent.uid,
              },
              'domains': {
                'table_name': 'domains',
                'table_field': 'agent',
                'table_value': agent.uid,
              },
              'leads_config': {
                'table_name': 'leads_config',
                'table_field': 'user_id',
                'table_value': agent.uid,
              },
              'media': {
                'table_name': 'media',
                'table_field': 'author',
                'table_value': agent.uid,
              },
              'contact_searched_listings': {
                'table_name': 'contact_searched_listings',
                'table_field': 'user_id',
                'table_value': agent.uid,
              },
              'testimonials': {
                'table_name': 'testimonials',
                'table_field': 'user_id',
                'table_value': agent.uid,
              },

              'agent_subscription': {
                'table_name': 'agent_subscription',
                'table_field': 'UserId',
                'table_value': agent.agent_id,
              },
              'pages': {
                'table_name': 'pages',
                'table_field': 'agent_id',
                'table_value': agent.agent_id,
              },
              'contacts': {
                'table_name': 'contacts',
                'table_field': 'agent_id',
                'table_value': agent.agent_id,
              },
              'home_options': {
                'table_name': 'home_options',
                'table_field': 'agent_id',
                'table_value': agent.agent_id,
              },
              'core_activities': {
                'table_name': 'core_activities',
                'table_field': 'agent_id',
                'table_value': agent.agent_id,
              },
              'property_activated': {
                'table_name': 'property_activated',
                'table_field': 'list_agentId',
                'table_value': agent.agent_id,
              },
              'receipt': {
                'table_name': 'receipt',
                'table_field': 'agent_id',
                'table_value': agent.agent_id,
              },
              'agent_ip': {
                'table_name': 'agent_ip',
                'table_field': 'agent_id',
                'table_value': agent.agent_id,
              },
            };
            axios.all(
              [
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.users),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.account_meta),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.homepage_properties),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.pricing),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.freemium),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.domains),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.leads_config),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.contact_searched_listings),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.testimonials),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.agent_subscription),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.pages),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.contacts),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.home_options),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.core_activities),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.property_activated),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.receipt),
                axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data/`,system_params.agent_ip),
              ]
            )
            .then(
              axios.spread(function (
                users,
                account_meta,
                homepage_properties,
                pricing,
                freemium,
                domains,
                leads_config,
                contact_searched_listings,
                testimonials,
                agent_subscription,
                pages,
                contacts,
                home_options,
                core_activities,
                property_activated,
                receipt,
                agent_ip
              ) {

                  window.parent.location = window.parent.location.href;
                $('.right_col').css('min-height', $(document).height() + 100);
                $('.left_col').css('min-height', $(document).height());
              })
            );
          }
        },
        {
          label: 'No'
        }]
    });

  }

  /*
    Cancel Agent Premium Subscription And Go Back To Freemium
  */
  handleCancelAgent = (event) => {

    event.preventDefault();

    const { agent } = this.state;

    confirmAlert({
      title: 'CANCEL SUBSCRIPTION',
      message: `Are you sure you want to cancel ${agent.full_name}\'s stripe subscription plan?`,
      buttons: [
        {
          label: 'CANCEL RIGHT NOW',
          onClick: () => { this.processCancelSubscription(agent, true) }
        },
        {
          label: 'CANCEL UNTIL PERIOD ENDS',
          onClick: () => { this.processCancelSubscription(agent, false) }
        },
        {
          label: 'NO'
        }
      ]
    });

  }

  /*
   * Process cancellation in stripe
   *  has two cancellation options. cancel immediately or cancel until payment period ends
   *
   * @params Object (agent) Bool (immediateCancel)
   * @return void
   */
  processCancelSubscription = (agent, immediateCancel) => {

    this.setState({
      processing: true
    });

    axios.post(`${hiveConfig.ace.apiURL}cancel-stripe-subscription`, { 
      'user_id': agent.uid, immediateCancel 
    })
    .then((response) => {

      if(response.data.noStripe) { // If agent doesn't have a record in stripe, cancel immediately and provide note.

        Alert.success('User\'s subscription now back to freemium! TAKE NOTE: This agent has no record in stripe, its subscription was cancelled immediately!', {
          timeout: 'none'
        });

        $(`#${agent.uid}-paying_customer`).html('<span class="label label-danger">No</span>');

          const agentNewData = {
            uid: agent.uid,
            full_name: agent.full_name,
            agent_id: agent.agent_id,
            freemium_plan_id: 1
          };

          this.setState({
            agent: agentNewData,
            processing: false
          });

      } else {

        if(immediateCancel) {

          Alert.success('User\'s subscription now back to freemium!');
          $(`#${agent.uid}-paying_customer`).html('<span class="label label-danger">No</span>');

          const agentNewData = {
            uid: agent.uid,
            full_name: agent.full_name,
            agent_id: agent.agent_id,
            freemium_plan_id: 1
          };

          this.setState({
            agent: agentNewData,
            processing: false
          });

        } else {

          Alert.success('Cancellation Success! User\'s subscription will be back to freemium until the end of billing cycle!');
          this.setState({processing: false});

        }

      }

    })
    .catch(error => {
      Alert.error(error);
      this.setState({processing: false});
    });

  }

  cancelButton = (full_name) => {

    let cancel_button;

    if(this.state.processing)
      cancel_button = (<button className='btn btn-warning' disabled>Processing...</button>);
    else
      cancel_button = (<button className='btn btn-warning' onClick={this.handleCancelAgent}>Cancel {`${full_name}'s subscription`}</button>);

    return cancel_button;

  };

  render() {

    const { full_name, isPaid } = this.state.agent;

    return (
      <Fragment>
        <button className='btn btn-danger' onClick={this.handleDeleteAgent}>Delete { full_name }</button>
        { isPaid ? this.cancelButton(full_name) : '' }
      </Fragment>
    );

  }

}

export default DeleteCancelAgent;