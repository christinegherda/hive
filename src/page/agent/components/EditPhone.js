import React, { Component, Fragment } from 'react';
import {InputGroup, FormControl} from 'react-bootstrap';
import axios from 'axios'
import $ from 'jquery';
import Alert from 'react-s-alert';
const hiveConfig = require('../../../hive.config.js');

class EditPhone extends Component {

  constructor(props) {

    super(props);

    this.state = {
      phoneFields: 0,
      phoneNumbers : this.props.phones
    }

  }

  addMultiple(type){

    if(this.state.phoneNumbers.length >= 3) {
      Alert.error(`Phone numbers limit exceeded!`);
    } else {
      this.setState({
        phoneNumbers: [...this.state.phoneNumbers, ""]
      })
    }

  }

  handleChange(e, index) {
    this.state.phoneNumbers[index] = e.target.value
    this.setState({phoneNumbers:this.state.phoneNumbers});
  }

  handleRemove(index) {

    if(index === 0) {
      Alert.error('Default phone number can only be editted, but cannot be remove.');
    } else {
      console.log('handle remove index', index);
      this.state.phoneNumbers.splice(index, 1);
      this.setState({phoneNumbers : this.state.phoneNumbers})
      console.log('phone numbers state', this.state.phoneNumbers);
    }
  }

  savePhone(e) {

    var extra = [];

    this.state.phoneNumbers.map((number, index) => {
      if(index !== 0) {
        extra.push(number);
      }
    });

    const params = {
      user_id: this.props.userId,
      defaultPhone: this.state.phoneNumbers[0],
      extraPhones: extra
    }

    axios.post(`${hiveConfig.ace.apiURL}add-user-phone`, params)
    .then(result => {
      this.props.handlestatechanges({
        uid: this.props.userId,
        phone: this.state.phoneNumbers[0]
      });
      Alert.success(result.data.message);
    })
    .catch(error => {
      Alert.error(error);
    });

  }

  render() {

    return (
      <Fragment>
        <li className="editphone-component">Phone Numbers: 

          <div className="form-group">
            {
              this.state.phoneNumbers.map((number, index)=>{
                return (
                  <div className="input-group-phone" key={index}>
                    <input type="text" className="form-control form-underline" value={number} onChange={(e) => this.handleChange(e, index)}  />
                    <button className="btn btn-danger" onClick={()=>this.handleRemove(index)}><i className="fa fa-trash"></i></button>
                  </div>
                )
              })
            }
            
            <button className="btn btn-primary" onClick={(e) => this.addMultiple(e)}><i className="fa fa-plus"></i></button>
            <button className="btn btn-success" onClick={(e) => this.savePhone(e)}><i className="fa fa-save"></i></button>
            
          </div>
        </li>
      </Fragment>
    );

  }

}

export default EditPhone;