import React from "react";
import $ from 'jquery';
import axios from 'axios';
import { Tooltip, OverlayTrigger} from 'react-bootstrap';
const hiveConfig = require('../../../hive.config.js');

export default class SendEmail extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            ace_dashboard_url: 'https://dashboard.agentsquared.com/',//(process.env.AGENT_DASHBOARD_URL) ? (process.env.AGENT_DASHBOARD_URL) : 'http://localhost:30004/',
            ace_port: (process.env.ACE_API_PORT) ? (process.env.ACE_API_PORT) : 3011,
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event) {
        const button = $(event.target);
        button.text("Loading..");

        const email = event.target.getAttribute('data-email');
        const name = event.target.getAttribute('data-agent');
        const mls = event.target.getAttribute('data-mls');
        const url = (mls === "ARMLS") ? 'https://armls.flexmls.com/ticket' : this.state.ace_dashboard_url+"idx_login";
        const msg = (mls === "ARMLS") ? 'FlexMLS account. Click the menu tab in the corner left, then click AgentSquared - Instant IDX Website' : 'Spark account using your FlexMLS login';

        var email_data = {
            recipient: email,
            sender: {
                email:'admin@agentsquared.com',
                name:'AgentSquared Admin'
            },
            subs: {
                first_name:name,
                content_msg:msg,
                relaunch_url:url

            },
            template_id:'49d31025-6527-46a0-8741-986d6a718099'
        };

        axios.post(`${hiveConfig.ace.apiURL}send-email/`, email_data)
        .then((response) => {
            button.removeClass("btn-red").addClass("btn-green").text("Email Sent").attr("disabled", "disabled");
        })
        .catch(function (error) {
            console.log(error);
        });

    }

    render() {
        localStorage.clear();
        const agent = this.props.agent;
        let tooltip = <Tooltip placement="top" id="tooltip-top">Send Email Relaunch</Tooltip>;
        const agent_email = agent.email;//process.env.REACT_APP_ENV !== 'production' ? 'christine.gonx@gmail.com' : agent.email;
        return (
            <OverlayTrigger placement="top" overlay={tooltip}>
                <button type="button" className="button btn-red" onClick={this.handleClick} data-agent={agent.full_name} data-mls={agent.mls_name} data-email={agent_email}> LOST </button>
            </OverlayTrigger> 
        );
    }
}