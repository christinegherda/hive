import React from "react";
import { Link } from "react-router-dom";
import {BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Modal, Tooltip, OverlayTrigger} from 'react-bootstrap';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import '../../../assets/css/react-confirm-alert.css';

import Switch from "react-switch";
import $ from 'jquery';
import axios from 'axios'
import Alert from 'react-s-alert';
const hiveConfig = require('../../../hive.config.js');
const auth = require('../../../session/auth.js');
var dateFormat = require('dateformat');
var now = new Date();

export default class MoreDetail extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleDashboadModal = this.handleDashboadModal.bind(this);
    this.handleDashboardModalClose = this.handleDashboardModalClose.bind(this);
    this.handleReceiptModal = this.handleReceiptModal.bind(this);
    this.handleReceiptModalClose = this.handleReceiptModalClose.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.linkFormat = this.linkFormat.bind(this);
    this.tokenStatusFormat = this.tokenStatusFormat.bind(this);
    this.addReceiptRecord = this.addReceiptRecord.bind(this);
    this.deleteAgent = this.deleteAgent.bind(this);
    this.agentUpdateCC = this.agentUpdateCC.bind(this);
    this.handleUpdateCC = this.handleUpdateCC.bind(this);
    this.state = {
      show: false,
      showDashboard: false,
      showReceipt: false,
      checked: false,
      receipts: null,
      showReceiptLoader: false,
      role: null,
      agent: null,
      updateCC: this.props.agent.freemium_cc_update
    };
  }

  componentWillMount() {
    const agentInfo = this.props.agent;
    this.setState({
      checked: agentInfo.paid_amount ? true : false
    });

    
   
  }
  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    let currentComponent = this;
    auth.checkAuthUser().then(function(res) {
      if(res.length) {
        // True
        currentComponent.setState({
          role     : res[0].role,
        });
      }
    }).then(
        currentComponent.getAgentReceipts({ agent_id: this.props.agent.uid }).then(function (e) {
            currentComponent.setState({
                receipts: e.data,
                show: true,
                agent: currentComponent.props.agent
            });
            console.log("Payment");
            console.log(e.data);
        })
    );

    // this.getAgentReceipts({ agent_id: this.props.agent.uid }).then(function (e) {
    //     currentComponent.setState({
    //         reciepts: e.data,
    //         show: true,
    //         agent: currentComponent.props.agent
    //     });
    //     console.log(e.data);
    // });
  }

  handleDashboadModal() {
    this.setState({ showDashboard: true });
  }

  handleDashboardModalClose() {
    this.setState({ showDashboard: false });
  }

  handleReceiptModal(checked) {
    let currentComponent = this;

    
    if (checked) {
        console.log(checked);
      $(".paying-customer").text("Paid"); $(".paying-customer").text("Paid");
        axios.all([
            this.getAgentReceipts({ agent_id: this.props.agent.uid })
        ]).then(axios.spread(function (receipt) {
            currentComponent.setState({
                receipts: receipt.data,
                show: true
            });
            
            // console.log(currentComponent.state.receipts);
        })).then(function (e) {
            // console.log("Done");
        });
    } else {
        // Update Records
        $(".paying-customer").text("Free");
        const urlData = new URL(window.location.href);
        var text_filter = this.state.search;
        if (urlData.searchParams.get('filter')) {
            text_filter = urlData.searchParams.get('filter');
        }
        confirmAlert({
            title: `Downgrade Agent`,
            message: `Are you sure to proceed to downgrade?`,
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => {

                        const params = {
                            'uid': this.props.agent.uid,
                        };
                        
                        axios.post(`${hiveConfig.ace.apiURL}downgrade-agent/`, params)
                        .then((response) => {
                            // Response after downgrade
                            this.getAgentReceipts({ agent_id: params.uid }).this(function(e) {
                                Alert.success('User Downgraded!');
                                currentComponent.setState({
                                    receipts: [],
                                    show: true
                                });
                            });
                            console.log(response);
                            
                            // window.parent.location = window.parent.location.origin + `/agent?filter=${text_filter}`;
                        })
                        .catch(function (error) {
                            console.log(error.response);
                        });
                }
                },
                {
                    label: 'No'
                }]
        });
    }

    
    currentComponent.setState({ checked });
    setTimeout(function(){
      currentComponent.setState({
        show: (checked) ? false : true,
        showReceipt: (checked) ? true : false,
        checked: false
      });
    }, 500);



  }
  handleReceiptModalClose() {
    let currentComponent = this;
    currentComponent.setState({
        showReceipt: false,
        show: true,
    });
  }
  getAgentReceipts(params) {
    return axios.post(`${hiveConfig.ace.apiURL}fetch-agent-receipts`, params);
  }
  updateSFdata(params) {
    return axios.post(`${hiveConfig.ace.apiURL}update-lead`, params);
  }

  addReceiptRecord(e) {
    e.preventDefault();
    let currentComponent = this;

    var valid = true,
        message = '';

    $(".error, .alert").remove();
    $('#addReceiptForm input').each(function() {
        var $this = $(this);

        if(!$this.val()) {
            var inputName = $this.closest('label');
            valid = false;
            message += 'Please enter your ' + inputName + '\n';
        }
        if(!valid) {
            $this.after('<p class="error">This field is required.</p>');
        }
    });

    if (valid) {

      const data = $("#addReceiptForm").serialize();
      const agentID = $("#addReceiptForm .agent_id").val();

      currentComponent.setState({
        showReceiptLoader: true,
      });

      const urlData = new URL(window.location.href);
      var text_filter = this.state.search;
      
      if (urlData.searchParams.get('filter')) {
        text_filter = urlData.searchParams.get('filter');
      }

      axios.post(`${hiveConfig.ace.apiURL}add-agent-receipts`, data).then(function(results) {

        // split our query string into its component parts
        var arr = data.split('&');
        var obj = {};

        for (var i=0; i<arr.length; i++) {

          var a = arr[i].split('=');
          var paramNum = undefined;
          var paramName = a[0].replace(/\[\d*\]/, function(v) {
            paramNum = v.slice(1,-1);
            return '';
          });

          var paramValue = typeof(a[1])==='undefined' ? true : a[1];

          paramName = paramName.toLowerCase();
          paramValue = paramValue.toLowerCase();

          if (obj[paramName]) {
            if (typeof obj[paramName] === 'string') {
              obj[paramName] = [obj[paramName]];
            }
            if (typeof paramNum === 'undefined') {
              obj[paramName].push(paramValue);
            } else {
              obj[paramName][paramNum] = paramValue;
            }
          } else {
            obj[paramName] = paramValue;
          }
        }

        // Refresh?
        $('#addReceiptForm').before("<div class='alert alert-info' role='alert'><i class='fa fa-cog fa-spin'></i> Adding Receipt Please wait...</div>");
         axios.all([
            currentComponent.getAgentReceipts({agent_id: currentComponent.props.agent.uid}),
            currentComponent.updateSFdata({
              plan_type: obj.product_type, //monthly yearly or manual
              amount: obj.amount, //99 or 990
              product_type: obj.type,
              user_id: currentComponent.props.agent.uid,
              full_name: currentComponent.props.agent.full_name,
              phone: currentComponent.props.agent.phone,
              email: currentComponent.props.agent.email,
              domains: currentComponent.props.agent.domains,
              subdomain_url: currentComponent.props.agent.subdomain_url
            })
          ]).then(axios.spread(function(receipt) {
            currentComponent.setState({
              show: false,
              showReceipt: true,
              receipts: receipt.data,
              showReceiptLoader: true,
              checked: true
            });
            $('#addReceiptForm').before("<div class='alert alert-success' role='alert'><i class='fa fa-check'></i> Successfully added Receipt</div>");
            console.log($(".paying-customer").text());
            $(".paying-customer").text("Paid");

            $("input[name='receipt_details'], input[name='transaction_details'], input[name='amount']").val('');
            // console.log(currentComponent.state.receipts);
            Alert.success('Agent Upgraded!');
            //   window.parent.location = window.parent.location.origin + `/agent?filter=${text_filter}`;
          })).then(function(e) {
            console.log("Done");
          }).catch(function(error) {
              Alert.error(error);
            //   window.parent.location = window.parent.location.origin + `/agent?filter=${text_filter}`;
          });
          //
          


      }).catch(function(error) {
        console.log(error);
        Alert.error(error);
        //   window.parent.location = window.parent.location.origin + `/agent?filter=${text_filter}`;
      });
    }
  }


  linkFormat(link) {
   if (!/^(f|ht)tps?:\/\//i.test(link)) {
      link = "http://" + link;
   } else {
      link = link;
   }
    return (
        <a target='_blank' href={link}>{link}</a>
    );
  }

  domainLinkFormat(ssl, link) {
    return (
        <a target='_blank' href={(ssl) ? "https://"+link : "http://"+link}>{link}</a>
    );
  }

  subDomainLinkFormat(ssl, link) {
    return (
        <a target='_blank' href={link}>{link}</a>
    );
  }

  tokenStatusFormat(status, agent) {
    if (status || agent.bearer_token) {
        return (
            <span> LIVE </span>
        );
    } else {
        return (
            <span className='label label-danger'>LOST</span>
        );
    }
  }
  receiptAmount(cell, receipt) {
    let amount = receipt.amount;
       return (
           <div> <p> $ { amount > 1000 ? amount / 100 : amount } </p></div>
       );
  }
  receiptCreated(cell, receipt) {
    let date = receipt.created_on;
    let date2 = new Date( date *1000);
       return (
        <div> 
          <p> {dateFormat(date2, "mmmm d, yyyy")}</p>
        </div>
       );
  }
  deleteAgent(agent) {
    // console.log(agent);
      const currentComponent = this;
    confirmAlert({
        title: `Delete Agent`,
        message: `Are you sure to delete ${agent.full_name}?`,
        buttons: [
        {
            label: 'Yes',
            onClick: () => {

              const params = {
                'uid': agent.uid,
                'agent_id': agent.agent_id,
              };

                axios.post(`${hiveConfig.ace.apiURL}delete-agent/`, params)
                .then((response) => {
                    window.parent.location = window.parent.location.origin + `/agent?filter=${currentComponent.props.search}`;
                })
                .catch(function (error) {
                    console.log(error.response);
                });
            }
        },
        {
            label: 'No'
        }]
    });
    // return axios.post(`${hiveConfig.ace.apiURL}delete-agent`, params);
  }

  handleUpdateCC(value) {

    let i = value ? 1 : 0;
    let currentComponent = this;

    let data = {
        'id':currentComponent.props.agent.uid,
        'updateCC':i
    }

    axios.post(`${hiveConfig.ace.apiURL}update-cc`, data).then(response=> {
      if (response.data.success) {
       currentComponent.setState({
          updateCC : response.data.response[0].require_cc_update
        })
      }
    })
    .catch(error=>{
      console.log(error);
    });


  }

  agentUpdateCC(agent) {
    return (
        <div className="col pull-right">
          <h5 className="update-cc"> 

            <b>Update Credit Card </b>
            <span>
              <Switch
                  onChange={this.handleUpdateCC}
                  checked={this.state.updateCC}
                  id="toggle-switch"
              />
            </span>
          </h5> 
        </div>
    )
  }
  render() {
    
    const agentInfo = this.props.agent;
    let modalTooltip = <Tooltip placement="top" id="tooltip-top">Agent Payment Details</Tooltip>;
    let dashboardTooltip = <Tooltip placement="top" id="tooltip-top">Login to Dashboard</Tooltip>;
   
    let tokenData = agentInfo.bearer_token ? agentInfo.bearer_token : agentInfo.access_token;
    let dashboard_sso_url = (process.env.AGENT_DASHBOARD_URL ? process.env.AGENT_DASHBOARD_URL : "https://dashboard.agentsquared.com/") + `Sales_auth/authenticate/?agent_id=${agentInfo.agent_id}&token=${tokenData}`;
    let agent_receipts = this.state.receipts;
      
    const months_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const valid = (new Date(agentInfo.paid_date)).getTime() > 0;
    const paid_date = new Date(agentInfo.paid_date);
    const paid_date_hours = paid_date.getHours();
    const paid_date_minutes = "0" + paid_date.getMinutes();
    const paid_date_seconds = "0" + paid_date.getSeconds();
    const paid_date_year = paid_date.getFullYear();
    const paid_date_month = months_arr[paid_date.getMonth()];
    const paid_date_day = paid_date.getDate();

    var paid_date_converted = agentInfo.paid_date;
    if (valid) {
        paid_date_converted = paid_date_month + ' ' + paid_date_day + ',' + paid_date_year + ' ' + paid_date_hours + ':' + paid_date_minutes.substr(-2) + ':' + paid_date_seconds.substr(-2);
    }

    var planName = '';
    var plan_occurence = agentInfo.paid_occurence;
    var paid_amount = agentInfo.paid_amount;
    
    // check if occurence(manual, yearly, monthly) 
    if(plan_occurence == 'manual'){
        if(paid_amount > 99){
          planName = 'Premium Yearly';
        }else{
          planName = 'Premium Monthly';
        }
    }else if(plan_occurence == 'yearly'){  
        planName = 'Premium Yearly';
    }else if (agentInfo.freemium_name && plan_occurence =='monthly') {
        //planName += agentInfo.freemium_name + ((agentInfo.PricingPlan && agentInfo.PricingPlan > 0) ? ' - ' + agentInfo.PricingPlan : ' ');
        planName = 'Premium Monthly'; 
    }else {
        // No Freemium
        // paid_amount, paid_currency, paid_type, paid_occurence
        if (agentInfo.paid_amount > 0) {
            planName += ((agentInfo.paid_amount > 100 && agentInfo.IsFromSpark == 0) ? agentInfo.paid_amount / 100 : agentInfo.paid_amount) + ' ' + agentInfo.paid_currency.toUpperCase() + ' ' + agentInfo.paid_type.toUpperCase() + ' ' + agentInfo.paid_occurence.toUpperCase();
        }
        else {
            planName += 'Freemium';
        }
      
    }

    let role = this.state.role;
    return (
      <div>
        <OverlayTrigger placement="top" overlay={modalTooltip}>
          <button className="button btn-blue more-details modal-target_1" onClick={this.handleShow}><i className="fa fa-dollar"></i></button>
        </OverlayTrigger> &nbsp;
        <OverlayTrigger placement="top" overlay={dashboardTooltip}>
          <button className="button btn-green" onClick={this.handleDashboadModal}><i className="fa fa-sign-out"></i></button>
        </OverlayTrigger>
        <Modal show={this.state.showDashboard} onHide={this.handleDashboardModalClose} className="modal_hive modal-dashboard">
          <Modal.Header closeButton>Go to Dashboard</Modal.Header>
          <Modal.Body>
            <div className="text-center">
              <h5>You are attempting to open Agent Dashboard, click on the button below to proceed</h5>
              <a href={dashboard_sso_url} target="_blank" type="button" className="btn btn-lg btn-green" ><i className="fa fa-sign-out"></i> Open Dashboard</a>
            </div>
          </Modal.Body>
        </Modal>
        
        <Modal show={this.state.showReceipt} onHide={this.handleReceiptModalClose} className="modal_hive modal-dashboard">
            <Modal.Header closeButton>Receipts</Modal.Header>
            <Modal.Body>
                <div className="receipt-details">
                    <form className="form-horizontal form-label-left" id="addReceiptForm">
                        <input type="hidden" name="agent_id" className="agent_id" value={agentInfo.uid} />
                        <input type="hidden" name="currency" value="usd" />
                        <input type="hidden" name="created_on" value={Math.floor(now / 1000)} />
                        <div className="form-group">
                            <label className="control-label col-md-3 col-sm-3 col-xs-12">Name of Agent
                    </label>
                            <div className="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" className="form-control col-md-7 col-xs-12" value={agentInfo.full_name} disabled />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-md-3 col-sm-3 col-xs-12" >Receipt Detail
                    </label>
                            <div className="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="receipt_details" className="form-control col-md-7 col-xs-12" required />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-md-3 col-sm-3 col-xs-12">Transaction Detail</label>
                            <div className="col-md-6 col-sm-6 col-xs-12">
                                <input className="form-control col-md-7 col-xs-12" type="text" name="transaction_details" required />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-md-3 col-sm-3 col-xs-12">Amount</label>
                            <div className="col-md-6 col-sm-6 col-xs-12">
                                <input className="form-control col-md-7 col-xs-12 amount" type="number" name="amount" required />
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-md-3 col-sm-3 col-xs-12">Product</label>
                            <div className="col-md-6 col-sm-6 col-xs-12">
                                <select name="type" className="form-control col-md-7 col-xs-12">
                                    <option value="idx">Agent IDX Website</option>
                                    <option value="spw">Single Property Listing</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-md-3 col-sm-3 col-xs-12">Product Type</label>
                            <div className="col-md-6 col-sm-6 col-xs-12">
                                <select name="product_type" className="form-control col-md-7 col-xs-12">
                                    <option value="manual">Manual</option>
                                    <option value="monthly">Monthly</option>
                                    <option value="yearly">Yearly</option>
                                </select>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
                                <button type="submit" className="btn btn-success" onClick={this.addReceiptRecord}>Add Receipt</button>
                            </div>
                        </div>

                    </form>

                    <div className="ln_solid"></div>
                    <h4> Receipts Record </h4>
                    <div className="table-responsive">
                        <BootstrapTable data={agent_receipts} striped hover>
                            <TableHeaderColumn width="70" isKey dataField='rid'>Receipt ID</TableHeaderColumn>
                            <TableHeaderColumn width="200" dataField='receipt_details'>Receipt Details</TableHeaderColumn>
                            <TableHeaderColumn width="70" dataFormat={this.receiptAmount} dataField='amount'>Amount</TableHeaderColumn>
                            <TableHeaderColumn width="70" dataField='type'>Type</TableHeaderColumn>
                            <TableHeaderColumn width="70" dataField='product_type'>Payment Type</TableHeaderColumn>
                            <TableHeaderColumn width="100" dataFormat={this.receiptCreated} dataField='created_on'>Date Created</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>

            </Modal.Body>
        </Modal>

        <Modal show={this.state.show} onHide={this.handleClose} className="modal_hive small">
          <Modal.Header closeButton>
                <Modal.Title className="text-center">Payment Information</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">

                <div className="col">
                    <div className="agent-more-info">
                        <table className="table table-bordered table-condensed active">
                            <thead>
                                <tr>
                                    <th>Plan</th>
                                    <th>Paying Customer</th>
                                    <th>Payment Type</th>
                                    <th>Last Payment</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        {planName}
                                    </td>
                                    <td>{agentInfo.paid_amount ? "YES" : "No"}</td>
                                    <td>{agentInfo.paid_amount ? "Paid" : "Free"} </td>
                                    <td>
                                        {agentInfo.paid_amount ? paid_date_converted : "Not Applicable"}
                                    </td>
                                    <td>
                                        <Switch
                                            onChange={this.handleReceiptModal}
                                            checked={this.state.checked}
                                            id="normal-switch"
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr />
                        <h4 className="text-center">Payment History</h4>
                       
                        <BootstrapTable data={agent_receipts} striped hover>
                            <TableHeaderColumn width="70" isKey dataField='rid'>Receipt ID</TableHeaderColumn>
                            <TableHeaderColumn width="200" dataField='receipt_details'>Receipt Details</TableHeaderColumn>
                            <TableHeaderColumn width="70" dataFormat={this.receiptAmount} dataField='amount'>Amount</TableHeaderColumn>
                            <TableHeaderColumn width="70" dataField='type'>Type</TableHeaderColumn>
                            <TableHeaderColumn width="70" dataField='product_type'>Payment Type</TableHeaderColumn>
                            <TableHeaderColumn width="100" dataFormat={this.receiptCreated} dataField='created_on'>Date Created</TableHeaderColumn>
                        </BootstrapTable>
                    </div>
                </div>
                <hr/>

                 { (role === "admin") ? this.agentUpdateCC(agentInfo) : "" }
            </div>
          </Modal.Body>

        </Modal>
      </div>
    );
  }
}
