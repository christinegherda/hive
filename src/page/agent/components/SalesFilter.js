import React, { Component, Fragment } from 'react';
import Select from 'react-select';
import axios from 'axios'
import $ from 'jquery';
const hiveConfig = require('../../../hive.config.js');

class SalesFilter extends Component {

  constructor(props) {

    super(props);

    this.state = {
      salesAgent: null,
      salesUsername: null,
      leadType: null
    };

    this.handleSalesSelect = this.handleSalesSelect.bind(this);
    this.handleLeadTypeSelect = this.handleLeadTypeSelect.bind(this);
  }

  handleSalesSelect = (event) => {

    // Send value to parent component
    this.props.handlestatechanges({
      sales_agent: event.value,
      sales_username: event.label,
      lead_type: this.state.leadType
    });

    // update state in current component
    this.setState({
      salesAgent: event.value,
      salesUsername: event.label
    })

  }

  handleLeadTypeSelect = (event) => {

    // Send value to parent component
    this.props.handlestatechanges({
      lead_type: event.value,
      sales_agent: this.state.salesAgent,
      sales_username: this.state.salesUsername
    });

    // update state in current component
    this.setState({
      leadType: event.value
    })

  }

  render() {

    let sales_select_placeholder = this.state.salesUsername ? this.state.salesUsername : 'Select Lead Owner';
    let leadType = this.state.leadType == 'paid' ? 'Close' : (this.state.leadType == 'free' ? 'Open' : 'Select Leads Status');
    const { isSales, salesAgents } = this.props;
    let sales_select = [];

    $.each(salesAgents, function(i, item) {
      sales_select.push(
        {
          value: item.id,
          label: item.username
        }
      );
    });

    return (
      <Fragment>
        <div className={ isSales ? 'salesOption' : 'filterOption' }>
          <div className='col-xs-8 col-sm-4 col-md-12'>
            <div className="col-md-1">
              <p className="filter-name">Sales Filter</p>
            </div>
            <div className="col-md-3">
              <Select
                name="sales_agent_select"
                placeholder={ sales_select_placeholder }
                onChange={ this.handleSalesSelect }
                options={ sales_select }
              />
            </div>
            <div className="col-md-3">
              <Select
                name=""
                placeholder={ leadType }
                onChange={ this.handleLeadTypeSelect }
                options={[
                  { value: 'paid', label: 'Close' },
                  { value: 'free', label: 'Open' },
                ]}
              />
            </div>
          </div>
        </div>
      </Fragment>
    );

  }

}

export default SalesFilter;