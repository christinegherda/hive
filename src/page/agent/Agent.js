import React from "react";
import {BootstrapTable, TableHeaderColumn, SearchField, ClearSearchButton } from 'react-bootstrap-table';
import {Modal, Button, Tooltip, OverlayTrigger} from 'react-bootstrap';
import axios from 'axios'
import $ from 'jquery';
import TopNav from '../../components/common/Topnav.js';
import Sidebar from '../../components/common/Sidebar.js';
import Footer from '../../components/common/Footer.js';
import PageTitle from '../../components/common/PageTitle.js';
import MoreDetailModal from './components/MoreDetailModal.js';
import SendEmail from './components/SendEmail.js';
import FreemiumPlanFormat from './components/FreemiumPlanFormat.js';
import DeleteCancelAgent from './components/DeleteCancelAgent.js';
import LeadAssignment from './components/LeadAssignment.js';
import SalesFilter from './components/SalesFilter.js';
import EditPhone from './components/EditPhone.js';
import Select from 'react-select';
import { confirmAlert } from 'react-confirm-alert';

const auth = require('../../session/auth.js');

const hiveConfig = require('../../hive.config.js');

var dateFormat = require('dateformat');

const title = [ 'Agents' ];

/*
 * Update specific row state from another component
 */
function updateState(datachange) {

  const { uid, lead_owner, lead_note, sales_username } = datachange;

  const rows = this.state.data;
  console.log(rows);
  const index = rows.findIndex((x) => x.uid === uid);
  console.log('index', index);

  if (index > -1 ) {
    rows[index].lead_owner = datachange.lead_owner;
    rows[index].lead_note = datachange.lead_note;
    rows[index].sales_username = datachange.sales_username;
    this.setState({
      data: rows
    });
  }

}

/*
 * Update state for sales agent filter
*/
function updateSalesState(filter) {

  this.setState({
    loading: true
  });

  console.log('sales filter: ', filter);
  let payment_status = filter.lead_type == 'paid' ? 1 : (filter.lead_type == 'free' ? 0 : this.state.filterPaymentStatus);

  const params = {
    'PricingPlan' : this.state.filterPricingPlan,
    'ApplicationType' : this.state.filterApplicationType,
    'IsFromSpark' : this.state.IsFromSpark,
    'EventType' : this.state.filterEventType,
    'MlsName' : this.state.filterMlsName,
    'Company' : this.state.filterCompany,
    'Token' : this.state.filterToken,
    'PaymentStatus' : payment_status,
    'search': this.state.search,
    'limit' :this.state.limit,
    'offset': 0,
    'sortItem' : this.state.sortItem,
    'sortOrder' : this.state.sortOrder,
    'leadOwner' : filter.sales_agent
  };

  console.log(params);

  axios.all([this.getTotalAgents(params), this.getAgents(params)])
  .then(axios.spread( (totalAgents, agents) => {

    this.setState({
      total: totalAgents.data[0].total,
      data: agents.data,
      loading: false,
      offset: 0,
      page: 1,
      filterLeadOwner: filter.sales_agent
    });

    $('.right_col').css('min-height', $(document).height() + 100);
    $('.left_col').css('min-height', $(document).height());

    window.scrollTo(0, 0);

  }))
  .catch(error => {
    console.log(error);
  });

}

function updateDefaultPhone(datachange) {

  const { uid, phone } = datachange;

  const rows = this.state.data;
  const index = rows.findIndex((x) => x.uid === uid);

  if (index > -1) {
    rows[index].phone = phone;
    this.setState({
      data: rows
    });
  }

}

export default class Agent extends React.Component {
  constructor(props) {
    super(props);
    const authStatus = auth.checkAuth().then(function(e) {
      if(e) {
        // True

      }
      else {
        // False, continue
        console.log("Un authenticated, please log in");
        window.location = "/";
      }
    });

    this.state = {
      data: null,
      loading: true,
      role: auth.getCookie('role'),
      page: 1,
      offset: 0,
      limit: 20,
      total: 0,
      search: '',
      FilterPaidPlanType: '', // Blank is All
      filters: {
        ApplicationType: '',
      },
      sortItem: 'uid',
      sortOrder: 'desc',
      paymentDetails: null,
      PricingPlan : null,
      ApplicationType : null,
      isFromSpark : null,
      EventType : null,
      agentOptions: [],
      filterPricingPlan: null,
      filterApplicationType: null,
      filterIsFromSpark: null,
      filterEventType: null,
      filterMlsName: null,
      filterCompany: null,
      filterToken: null,
      filterPaymentStatus: null,
      showNote: false,
      sales_agents: null, // sales agents from hive-users table
      filterLeadOwner: null, // lead owner id
    };

    this.filterClick = this.filterClick.bind(this);
    this.handleShowNote = this.handleShowNote.bind(this);
    this.handleHideNote = this.handleHideNote.bind(this);
    this.getPhones = this.getPhones.bind(this);

  }
  componentWillMount() {

    let currentComponent = this;
    // console.log(currentComponent.state.role);
    /*
      Fetching Algorithm
       - Fetch Total
       - Fetch Agents Based on limit
       - Initialize States
    */
    var params = {
      'limit': this.state.limit,
      'search': this.state.search,
      'offset': this.state.offset,
      'page': this.state.page,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
    };

    const urlData = new URL(window.location.href);

    var text_filter = this.state.search;

    // if url has search parameters
    if (urlData.searchParams.get('filter')) {

      text_filter = urlData.searchParams.get('filter');
      params.search = text_filter; // assign new value of search in params

    }

    // if user logged in is sales role, display only the leads that assigned to sales agent. except if the sales is performing search
    if(this.state.role == 'sales' && !urlData.searchParams.get('filter')) {

      params.leadOwner = auth.getCookie('id'); // hive-users id
      params.PaymentStatus = 0; // display only open leads / free agents

      this.setState({
        filterLeadOwner: auth.getCookie('id'),
        filterPaymentStatus: 0
      });

    }

    axios.post(`${hiveConfig.ace.apiURL}fetch-agents`, params).then(
      function (agents) {
        currentComponent.setState({
          data: agents.data,
          loading: false,
          search: text_filter
        });
        currentComponent.getTotalAgents(params).then(function (totalAgents) {
          currentComponent.setState({
            total: totalAgents.data[0].total,
            loading: false,
            search: text_filter
          });
        });
      }

    );

    axios.get(`${hiveConfig.ace.apiURL}fetch-sales-agent`)
    .then(sales => {
      this.setState({
        sales_agents: sales.data
      })
    })
    .catch(error => {
      console.log(error);
    })
  }
  removeAgentData(params) {
    return axios.post(`${hiveConfig.ace.apiURL}delete-agent-system-data`, params);
  }
  getProductOptions(params) {
    return axios.post(`${hiveConfig.ace.apiURL}fetch-product-options`, params);
  }
  getAgentOptions(params) {
    return axios.post(`${hiveConfig.ace.apiURL}fetch-agent-options`, params);
  }
  getAgents(params) {
    // const search = params.search ? params.search: '';
    return axios.post(`${hiveConfig.ace.apiURL}fetch-agents`, params);
  }
  getTotalAgents(params) {
    // const search = params.search ? params.search: '';
    return axios.post(`${hiveConfig.ace.apiURL}fetch-total-agents`, params);
  }
  sync_status(cell, agents){

      let synctooltip = <Tooltip placement="top" id="tooltip-top">Sync Now</Tooltip>;
      let sync_url = `https://dashboard.agentsquared.com/sales_auth/authenticate?agent_id=${agents.agent_id}&token=${agents.access_token}`;

      if(!agents.sync_status)
       return (
          <OverlayTrigger placement="top" overlay={synctooltip}>
            <a href={sync_url} target='_blank' className="button btn-warning"> <i className="fa fa-history"></i></a>
          </OverlayTrigger>
       )

  }
  enumFormatter(cell, row, enumObject) {
    return enumObject[cell] ? enumObject[cell] : '';
  }
  enumPurchaseFormatter(cell, row, enumObject) {

    if (row.cancel_reason) {
      return "Cancelled";
    }
    else {
      return enumObject[cell] ? enumObject[cell] : '';
    }

  }
  actionButtonFormatter(cell, agents){
       return (
        <div>
        <MoreDetailModal agent={agents} />
        </div>
       );
  }
  productNameFormat(cell, agents) {

    let product = (cell === 'Agent_IDX_Website') ? 'IDX' : 'SPW';

    return (
      <div> <p> {product} </p></div>
    );

  }
  _setTableOption(){
    if(this.state.loading){
     return "No Data Available";
    }else{
      return (
        <div>{this.fetchingDataLoader()}</div>
      )
    }
  }
  fetchingDataLoader() {
    let loader;
    if (this.state.loading) {
      loader =
        <div className="page-loader">
          <div className="loadertext"><i className="fa  fa-circle-o-notch fa-spin"></i> <br/> Fetching Data</div>
        </div>
    } else {
      loader = <div></div>
    }
    return (
        <div>{loader}</div>
    )
  }
  productNameFormatExport(cell, agents) {
    let _product = agents.ApplicationType;
    let product = _product.split("_").join(" ");
       return (product);
  }
  csvCellFormatter(cell, agents) {
    return cell ? cell: '';
  }
  onSizePerPageList(list) {

    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : this.state.filterEventType,
      'MlsName' : this.state.filterMlsName,
      'Company' : this.state.filterCompany,
      'Token' : this.state.filterToken ,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'search': this.state.search,
      'limit' : list,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder,
      'leadOwner': this.state.filterLeadOwner
    };
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
    .then(axios.spread(function (totalAgents, agents) {
      currentComponent.setState({
        total: totalAgents.data[0].total,
        data: agents.data,

        limit: list
      });


    })).then(function() {
      currentComponent.setState({
        loading: false,
      });
      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    });

  }
  onPageChange(number, size) {
    let currentComponent = this;

    currentComponent.setState({
      loading: true,
    });

    console.log('onPageChange number', number);
    console.log('onPageChange size', size);
    console.log('onPageChange limit state', this.state.limit);

    var params = {
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : this.state.filterEventType,
      'MlsName' : this.state.filterMlsName,
      'Company' : this.state.filterCompany,
      'Token' : this.state.filterToken ,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'limit': size,
      'search': this.state.search,
      'offset': (number * size) - size ,
      'page': number,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
      'leadOwner': this.state.filterLeadOwner
    };
    axios.post(`${hiveConfig.ace.apiURL}fetch-agents`, params)
    .then(function (response) {
      currentComponent.setState({
        data: response.data,
        page: number,
        limit: size,
        offset: number * size,

      });
    })
    .then(function() {
      currentComponent.setState({
        loading: false
      });
    })
    .catch(function (error) {
      console.log(error);
    });
    window.scrollTo(0, 0);
  }
  onSearchChange(searchText, colInfos, multiColumnSearch) {
    // Does nothing, but will be handled by trigger
  }
  handleKeyPressSearch(e) {
    // Does nothing, but will be handled by trigger

    console.log(e);
  }
  createCustomSearchField (props) {
    const urlData = new URL(window.location.href);
    var text_filter = this.state.search;
    if (urlData.searchParams.get('filter')) {
      text_filter = urlData.searchParams.get('filter');
    }
    return (
      <SearchField
          className='form-control' id='agentSearch'
          onKeyPress={(event) => {
              if (event.key === "Enter") {
                  this.handleSearchButtonClick(event)
                }
              }}
          placeholder='Search for User ID, Agent ID, Full name, Email, Phone, MLS, Token Status and Company'
        defaultValue={text_filter}
        />
    );
  }
  createCustomClearButton(onClick) {
    return (
      <ClearSearchButton
        btnText='Search'
        btnContextual='btn-primary'
        className='btn btn-primary'
        onClick={ e => this.handleSearchButtonClick(onClick) }/>
    );
  }
  handleSearchButtonClick (onClick) {
    let currentComponent = this;
    var searchText = document.getElementById("agentSearch").value; //$("#agentSearch").val();
    console.log('Trigger Search ', searchText);
    currentComponent.setState({
      loading: true
    });
    const params = {
      // 'FilterPaidPlanType' : this.state.FilterPaidPlanType,
      'search': searchText,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder
    };
    window.parent.location = window.location.origin + '/agent?filter=' + searchText;

  }
  tokenStatusFormat(cell, agents) {
    if (agents.is_valid || agents.bearer_token) {
      return "LIVE";
    } else {
      return(
        <SendEmail agent={agents}></SendEmail>
      )
    }

  }
  emailLink(cell, agents) {
    const email = agents.email;
    return(
      <a href={'mailto:'+email} target="_top">{email}</a>
    )
  }
  domainLink(cell, agents) {
    const url = agents.domains;
    return(
      <a href={'http://'+url} target="_blank">{url}</a>
    )
  }
  dateFormatter(cell, agents) {
    return dateFormat(cell, "mmmm d, yyyy hh:mm:ss")
  }
  onSortChange(sortItem, sortOrder){
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      // 'FilterPaidPlanType' : this.state.FilterPaidPlanType,
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : this.state.filterEventType,
      'MlsName' : this.state.filterMlsName,
      'Company' : this.state.filterCompany,
      'Token' : this.state.filterToken ,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : sortItem,
      'sortOrder' : sortOrder,
      'leadOwner': this.state.filterLeadOwner
    };


    axios.post(`${hiveConfig.ace.apiURL}fetch-agents`, params)
    .then(function (response) {
      currentComponent.setState({
        data: response.data,
        sortItem: sortItem,
        sortOrder: sortOrder,
        loading: false
      });
    })
    .catch(function (error) {
      console.log(error);
    });

  }
  filterClick(event){
    let currentComponent = this;
    /*
      Load Filter options
       - Agent
       - Product
    */
    currentComponent.setState({
      loading:true
    });
    axios.all([
      this.getAgentOptions({filter: 'mls_id'}),
      this.getAgentOptions({filter: 'company'})
    ]).then(axios.spread(function(mls_id, company) {
      return currentComponent.setState({
        agentOptions : {mls_id: mls_id.data , company: company.data}
      });
    })).then(function(e) {
      // console.log("Done");
      currentComponent.setState({
        loading: false
      });
    });
    axios.all([
      this.getProductOptions({filter: 'PricingPlan'}),
      this.getProductOptions({filter: 'ApplicationType'}),
      this.getProductOptions({filter: 'isFromSpark'}),
      this.getProductOptions({filter: 'EventType'})
    ]).then(axios.spread(function(PricingPlan, ApplicationType, isFromSpark, EventType) {

      return currentComponent.setState({
        PricingPlan     : PricingPlan.data,
        ApplicationType : ApplicationType.data,
        isFromSpark     : isFromSpark.data,
        EventType       : EventType.data,
      });
    })).then(function(e) {
      // console.log("Done");
      $(".filterOption, .btn-clear").toggleClass("show");
    });

  }
  clearFilter(event){
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    currentComponent.setState({
      filterPricingPlan: null,
      filterApplicationType: null,
      filterIsFromSpark: null,
      filterEventType: null,
      filterMlsName: null,
      filterCompany: null,
      filterToken: null,
      filterPaymentStatus: null,
      filterLeadOwner: null
    });

    var params = {
      'limit': this.state.limit,
      'search': this.state.search,
      'offset': this.state.offset,
      'page': this.state.page,
      'sortItem': this.state.sortItem,
      'sortOrder': this.state.sortOrder,
    };
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
      .then(axios.spread(function (totalAgents, agents) {
        // console.log(totalAgents);
        currentComponent.setState({
          total: totalAgents.data[0].total,
          data: agents.data,
          loading: false
        });

        $('.right_col').css('min-height', $(document).height() + 200);
        $('.left_col').css('min-height', $(document).height());
      }));

  }
  buildOptions(objectData) {
    var data = '';
    for (var key in objectData) {
      // skip loop if the property is from prototype
      if (!objectData.hasOwnProperty(key)) continue;

      var obj = objectData[key];
      for (var prop in obj) {
          // skip loop if the property is from prototype
          if(!obj.hasOwnProperty(prop)) continue;

          // your code
          // console.log(prop + " = " + obj[prop]);
          data += `<option>${obj[prop]}</option>`;
      }
    }
    return data;
  }
  updateValuePricingPlan (newValue) {
    console.log(newValue.value);
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      'PricingPlan' : newValue.value,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : this.state.filterEventType,
      'MlsName' : this.state.filterMlsName,
      'Company' : this.state.filterCompany,
      'Token' : this.state.filterToken ,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset': 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder,
      'leadOwner': this.state.filterLeadOwner
    };
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
    .then(axios.spread(function (totalAgents, agents) {
      // console.log(totalAgents);
      console.log(agents);
      currentComponent.setState({
        total: totalAgents.data[0].total,
        data: agents.data,
        loading: false,
        offset: 0,
        page: 1,
        filterPricingPlan: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));
  }
  updateValueApplicationType (newValue) {
    console.log(newValue.value);
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : newValue.value,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : this.state.filterEventType,
      'MlsName' : this.state.filterMlsName,
      'Company' : this.state.filterCompany,
      'Token' : this.state.filterToken,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset': 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder,
      'leadOwner': this.state.filterLeadOwner
    };
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
    .then(axios.spread(function (totalAgents, agents) {
      currentComponent.setState({
        total: totalAgents.data[0].total,
        data: agents.data,
        loading: false,
        offset: 0,
        page: 1,
        filterApplicationType: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));
  }
  updateValueIsFromSpark (newValue) {
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : newValue.value,
      'EventType' : this.state.filterEventType,
      'MlsName' : this.state.filterMlsName,
      'Company' : this.state.filterCompany,
      'Token' : this.state.filterToken,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder,
      'leadOwner': this.state.filterLeadOwner
    };
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
    .then(axios.spread(function (totalAgents, agents) {
      currentComponent.setState({
        total: totalAgents.data[0].total,
        data: agents.data,
        loading: false,
        offset: 0,
        page: 1,
        filterIsFromSpark: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));
    // this.setState({
    //   filterIsFromSpark: newValue
    //
    // });
  }
  updateValueEventType (newValue) {
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : newValue.value,
      'MlsName' : this.state.filterMlsName,
      'Company' : this.state.filterCompany,
      'Token' : this.state.filterToken,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder,
      'leadOwner' : this.state.filterLeadOwner
    };
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
    .then(axios.spread(function (totalAgents, agents) {
      currentComponent.setState({
        total: totalAgents.data[0].total,
        data: agents.data,
        loading: false,
        offset: 0,
        page: 1,
        filterEventType: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));
    // this.setState({
    //   filterEventType: newValue
    //
    // });
  }
  updateValueMlsName (newValue) {
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : this.state.filterEventType,
      'MlsName' : newValue.value,
      'Company' : this.state.filterCompany,
      'Token' : this.state.filterToken,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder,
      'leadOwner' : this.state.filterLeadOwner
    };
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
    .then(axios.spread(function (totalAgents, agents) {
      currentComponent.setState({
        total: totalAgents.data[0].total,
        data: agents.data,
        loading: false,
        offset: 0,
        page: 1,
        filterMlsName: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));
    // this.setState({
    //   filterMlsName: newValue
    // });
  }
  updateValueCompany (newValue) {
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : this.state.filterEventType,
      'MlsName' : this.state.filterMlsName,
      'Company' : newValue.value,
      'Token' : this.state.filterToken,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder,
      'leadOwner' : this.state.filterLeadOwner
    };
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
    .then(axios.spread(function (totalAgents, agents) {
      currentComponent.setState({
        total: totalAgents.data[0].total,
        data: agents.data,
        loading: false,
        offset: 0,
        page: 1,
        filterCompany: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));
    // this.setState({
    //   filterCompany: newValue
    // });
  }
  updateValueToken (newValue) {
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : this.state.filterEventType,
      'MlsName' : this.state.filterMlsName,
      'Company' : this.state.filterCompany,
      'Token' : newValue.value,
      'PaymentStatus' : this.state.filterPaymentStatus,
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder,
      'leadOwner' : this.state.filterLeadOwner
    };
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
    .then(axios.spread(function (totalAgents, agents) {
      currentComponent.setState({
        total: totalAgents.data[0].total,
        data: agents.data,
        loading: false,
        offset: 0,
        page: 1,
        filterToken: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));
    // this.setState({
    //   filterToken: newValue
    // });
  }
  updateValuePaymentStatus (newValue) {
    const currentComponent = this;
    currentComponent.setState({
      loading: true
    });
    const params = {
      'PricingPlan' : this.state.filterPricingPlan,
      'ApplicationType' : this.state.filterApplicationType,
      'IsFromSpark' : this.state.IsFromSpark,
      'EventType' : this.state.filterEventType,
      'MlsName' : this.state.filterMlsName,
      'Company' : this.state.filterCompany,         
      'Token' : this.state.filterToken,
      'PaymentStatus' : newValue.value,
      'search': this.state.search,
      'limit' :this.state.limit,
      'offset' : 0,
      'sortItem' : this.state.sortItem,
      'sortOrder' : this.state.sortOrder,
      'leadOwner' : this.state.filterLeadOwner
    };                                                      
    axios.all([this.getTotalAgents(params), this.getAgents(params)])
    .then(axios.spread(function (totalAgents, agents) {
      currentComponent.setState({
        total: totalAgents.data[0].total,
        data: agents.data,
        loading: false,
        offset: 0,
        page: 1,
        filterPaymentStatus: newValue.value
      });

      $('.right_col').css('min-height', $(document).height() + 100);
      $('.left_col').css('min-height', $(document).height());
    }));
    // this.setState({
    //   filterPaymentStatus: newValue
    // });
  }
  createCustomToolBar = props => {

    const { selectedOption } = this.state;
    const value = selectedOption && selectedOption.value;
    const pricingPlan = this.state.PricingPlan;
    const applicationType = this.state.ApplicationType;
    const source = this.state.isFromSpark;
    const eventType = this.state.EventType;
    const agentOpion = this.state.agentOptions;
    const agentMLS = agentOpion.mls_id;
    const agentCompany = agentOpion.company;
    let plan = [];
    let product = [];
    let sourcevia = [];
    let subscription = [];
    let mlsname = [];
    let company = [];
    let pricing = [];

    $.each(pricingPlan, function (i, item) {
      if (item.PricingPlan === 'monthly') {
        pricing.push(
          {
            value: item.PricingPlan,
            label: 'Premium Monthly',
          }
        )
      } else if (item.PricingPlan === 'yearly') {
        pricing.push(
          {
            value: item.PricingPlan,
            label: 'Premium Yearly',
          }
        )
      } else if (item.PricingPlan !== 'test') {
        pricing.push(
          {
            value: item.PricingPlan,
            label: item.PricingPlan,
          }
        )
      }
    });

    pricing.push(
      {
        value: 1,
        label: 'TRIAL',
      }
    )

    $.each(agentMLS, function (i, item) {
      if (item.mls_name) {
        mlsname.push(
          {
            value: item.mls_id,
            label: item.mls_name,
          }
        )
      }
    });

     $.each(agentCompany, function (i, item) {
      company.push(
        {
          value: item.company,
          label: item.company,
        }
      )
    });

    $.each(applicationType, function (i, item) {
      if(item.ApplicationType) {
        product.push(
          {
            value: item.ApplicationType,
            label: item.ApplicationType.replace(/_/g, ' ')
          }
        )
      }
    });

    $.each(source, function (i, item) {
        sourcevia.push(
          {
            value: item.isFromSpark,
            label: item.isFromSpark ? "Spark" : "AgentSquared"
          }
        )
    });

    $.each(eventType, function (i, item) {
      if(item.EventType) {
        subscription.push(
          {
            value: item.EventType,
            label: item.EventType.replace(/_/g, ' ')
          }
        )
      }
    });

    const {
      filterPricingPlan,
      filterApplicationType,
      filterIsFromSpark,
      filterEventType,
      filterMlsName,
      filterToken,
      filterCompany,
      filterPaymentStatus
      } = this.state;

    const pricingPlanValue = filterPricingPlan && filterPricingPlan.value;
    const ApplicationTypeValue = filterApplicationType && filterApplicationType.value;
    const IsFromSparkValue = filterIsFromSpark && filterIsFromSpark.value;
    const EventTypeValue = filterEventType && filterEventType.value;
    const MlsNameValue = filterMlsName && filterMlsName.value;
    const CompanyValue = filterCompany && filterCompany.value;
    const TokenValue = filterToken && filterToken.value;
    const PaymentStatusValue = filterPaymentStatus && filterPaymentStatus.value;

    const isSales = (this.state.role === "sales") ? true : false;
    let filter = null;
    let filterAdmin = null;

    if(isSales) {
      filter = <SalesFilter isSales = { isSales } salesAgents = { this.state.sales_agents } handlestatechanges={ updateSalesState.bind(this) } />
    } else {
      filterAdmin = <button className="button btn-blue btn-filter" onClick={this.filterClick}><i className="fa fa-search"></i> Search Filter </button>
      filter = <SalesFilter isSales = { isSales } salesAgents = { this.state.sales_agents } handlestatechanges={ updateSalesState.bind(this) } />
    }

    return (
      <div>
        <div className='col-xs-8 col-sm-4 col-md-1'>
        { props.components.btnGroup }
        </div>
        <div className='col-xs-8 col-sm-4 col-md-5'>
          {filterAdmin}
        </div>
        <div className='col-xs-8 col-sm-4 col-md-6'>
          { props.components.searchPanel }
        </div>
        <div className="filterOption">
          <div className='col-xs-8 col-sm-4 col-md-12'>
            <div className="col-md-1">
              <p className="filter-name">Product Filter</p>
            </div>
            <div className="col-md-3">
             <Select
              name=""
              placeholder="Plan"
              value={pricingPlanValue}
              onChange={this.updateValuePricingPlan.bind(this)}
              options={pricing}
             />
           </div> 
            <div className="col-md-3">
              <Select
                name=""
                placeholder="Product"
                value={ApplicationTypeValue}
                onChange={this.updateValueApplicationType.bind(this)}
                options={product}
              />
            </div>
            <div className="col-md-2">
              <Select
                name=""
                placeholder="Subscribe Via"
                value={IsFromSparkValue}
                onChange={this.updateValueIsFromSpark.bind(this)}
                options={sourcevia}
              />
            </div>
            <div className="col-md-3">
              <Select
                name=""
                placeholder="Subscription"
                value={EventTypeValue}
                onChange={this.updateValueEventType.bind(this)}
                options={subscription}
              />
            </div>
          </div>
        </div>
        <div className="filterOption">
          <div className='col-xs-8 col-sm-4 col-md-12'>
            <div className="col-md-1">
              <p className="filter-name">Agent Filter</p>
            </div>
            <div className="col-md-3">
              <Select
                name=""
                placeholder="MLS Name"
                value={MlsNameValue}
                onChange={this.updateValueMlsName.bind(this)}
                options={mlsname}
              />
            </div>
            <div className="col-md-3">
              <Select
                name=""
                placeholder="Token Status"
                value={TokenValue}
                onChange={this.updateValueToken.bind(this)}
                options={[
                        { value: '0', label: 'LOST' },
                        { value: '1', label: 'LIVE' },
                      ]}
              />
            </div>
            <div className="col-md-2">
              <Select
                name=""
                placeholder="Company"
                value={CompanyValue}
                onChange={this.updateValueCompany.bind(this)}
                options={company}
              />
            </div>
            <div className="col-md-3">
              <Select
                name=""
                placeholder="Payment Status"
                value={PaymentStatusValue}
                onChange={this.updateValuePaymentStatus.bind(this)}
                options={[
                        { value: '0', label: 'Cancelled' },
                        { value: '1', label: 'Purchased' },
                      ]}
              />
            </div>
          </div>
        </div>
        {filter}
        <div className="col-xs-8 col-sm-4 col-md-12 ">
          <div className="pull-right">
            <button className="button btn-warning btn-clear hide" onClick={this.clearFilter.bind(this)}><i className="fa fa-eraser"></i> Clear Filter </button>
          </div>
        </div>
      </div>
    );
  }

  freemiumPlanFormatter(cell, row, enumObject) {
    return (
      <React.Fragment>
        <FreemiumPlanFormat 
          row={
            {
              'uid': row.uid,
              'paid_occurence': row.paid_occurence,
              'paid_amount': row.paid_amount,
              'PricingPlan': row.PricingPlan,
              'IsFromSpark': row.IsFromSpark,
              'mls_id': row.mls_id,
              'freemium_plan_id': row.freemium_plan_id,
              'trial': row.trial,
              'IsFromSpark': row.IsFromSpark
            }
          }
        />
      </React.Fragment>
    )
  }

  redirectLink(link) {

    const pattern = /^((http|https|ftp):\/\/)/;

    if(!pattern.test(link)) {
      link = "http://" + link;
    }

    window.open(link, '_blank');

  };

  getPhones(user_id, defaultPhone) {

    var phones = [defaultPhone];

    axios.get(`${hiveConfig.ace.apiURL}user-phone-numbers/${user_id}`)
    .then(result => {
      for(var i = 0; i < result.data.length; i++) {
        var obj = result.data[i];
        phones.push(obj.phone_number);
      }
    })
    .catch(error => {
      console.log(error);
    })

    return phones;

  }

  expandComponent(row) {
    /*
      Execute API Call
      - Requirement
        - row.uid
        - row.agent_id

       Add Sign-up date ,Date Upgraded to Pro, Expiry Date
    */

    const months_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const rowData = row;
    const total_active = rowData.total_active ? rowData.total_active : "0";
    const total_sold = rowData.total_sold ? rowData.total_sold : "0";
    const web_leads = rowData.num_leads_web ? rowData.num_leads_web : "0";
    const flex_leads = rowData.num_leads_flex ? rowData.num_leads_flex : "0";
    //const paying_customer = rowData.paid_amount ? `<span class="label label-success">YES</span>` : `<span class="label label-danger">No</span>`;
    const recent_sold_listings = (rowData.recent_sold_date !== null && rowData.recent_sold_date !== '0000-00-00' && rowData.recent_sold_date !== '0000-00-00 00:00:00') ? dateFormat(rowData.recent_sold_date, "mmmm d, yyyy") : "Not Applicable";
    const recent_active_listings = (rowData.recent_active_date !== null && rowData.recent_active_date !== '0000-00-00' && rowData.recent_active_date !== '0000-00-00 00:00:00') ? dateFormat(rowData.recent_active_date, "mmmm d, yyyy") : "Not Applicable";
    //const token_status = rowData.is_valid ? `<span class="label label-success">LIVE</span>` : `<span class="label label-danger">LOST</span>`;


    const valid = (new Date(rowData.paid_date)).getTime() > 0;

    const paid_date = new Date(rowData.paid_date);
    const paid_date_hours = paid_date.getHours();
    const paid_date_minutes = "0" + paid_date.getMinutes();
    const paid_date_seconds = "0" + paid_date.getSeconds();
    const paid_date_year = paid_date.getFullYear();
    const paid_date_month = months_arr[paid_date.getMonth()];
    const paid_date_day = paid_date.getDate();



    var paid_date_converted = rowData.paid_date;
    if (valid) {
      paid_date_converted = paid_date_month + ' ' + paid_date_day + ',' + paid_date_year; // + ' ' + paid_date_hours + ':' + paid_date_minutes.substr(-2) + ':' + paid_date_seconds.substr(-2);
    }
    var payment_date_utc = paid_date_converted;
    var full_payment_date = new Date(paid_date_converted);
    full_payment_date.setDate(full_payment_date.getDate() - 1);
    paid_date_converted = months_arr[full_payment_date.getMonth()] + " " + full_payment_date.getDate() + "," + full_payment_date.getFullYear();
    // const utcDate = new Date(Date.UTC(paid_date_year, paid_date.getMonth(), paid_date_day, paid_date_hours, paid_date_minutes, paid_date_seconds));
    // console.log("Time Generated : ", paid_date_converted);
    var agent_photo = '<h1><i class="fa fa-user"></i></h1>';

    if (rowData.agent_photo) {
      agent_photo = `https://s3-us-west-2.amazonaws.com/agentsquared-assets/uploads/photo/${rowData.agent_photo}`;
    }
    // return (
    //   <div className="details-info text-center"><h3><i className="fa fa-spinner fa-spin"></i> Fetching Agent Details</h3></div>

    // );
    // var planName = '';
    // if (rowData.freemium_name) {
    //   planName += rowData.freemium_name + ((rowData.PricingPlan && rowData.PricingPlan > 0) ? ' - ' + rowData.PricingPlan : ' ');
    // }
    // else {
    //   // No Freemium
    //   // paid_amount, paid_currency, paid_type, paid_occurence
    //   if (rowData.paid_amount > 0) {
    //     planName += ((rowData.paid_amount > 100 && rowData.IsFromSpark == 0) ? rowData.paid_amount / 100 : rowData.paid_amount) + ' ' + rowData.paid_currency.toUpperCase() + ' ' + rowData.paid_type.toUpperCase() + ' ' + rowData.paid_occurence.toUpperCase();
    //   }
    //   else {
    //     planName += 'FREE';
    //   }
    // }


    let phoneNumbers = [rowData.phone]

    return (
      <div colSpan="12">
        <div className="details-info">
          <div className="row">
            <div className="col-md-4">
              <div className="alert alert-secondary">
                <div className="col-md-3 text-center">
                  {rowData.agent_photo ? <img src={agent_photo} alt="" className="agent-photo img-responsive" /> : <h1><i className="fa fa-user"></i></h1>}
                </div>
                <div className="col-md-9">
                  <h3 className="agent-name">{rowData.full_name}</h3>
                  <ul className="list-none">
                    <EditPhone
                      phones={this.getPhones(rowData.uid, rowData.phone)}
                      userId={rowData.uid}
                      handlestatechanges = { updateDefaultPhone.bind(this) }
                    />
                    <li>Email: <a href="mailto:{rowData.email}" className="agent-email">{rowData.email}</a></li>
                    <li>Sign up date: {dateFormat(rowData.signup_date, "mmmm d, yyyy hh:mm:ss")}</li>
                    <li>Expiry date: {paid_date_converted? '' : dateFormat(rowData.end_of_trial_date, "mmmm d, yyyy hh:mm:ss")}</li>
                    <li>Date Upgraded to Pro (System): {payment_date_utc ? payment_date_utc: ''}</li>
                    <li>Date Upgraded to Pro (SalesForce): {paid_date_converted ? paid_date_converted : ''}</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-md-2">
              <div className="row alert alert-info ">
                <div className="col-md-6 text-right">
                  <h1 className="badger"><i className="fa fa-heart"></i></h1>
                </div>
                <div className="col-md-6 text-left">
                  <h1 id={rowData.uid + "-total_active"}>{total_active}</h1>
                  <b>Active Listings</b>
                </div>
              </div>
            </div>
            <div className="col-md-2">
              <div className="row alert alert-success ">
                <div className="col-md-6 text-right">
                  <h1 className="badger"><i className="fa fa-strikethrough"></i></h1>
                </div>
                <div className="col-md-6 text-left">
                  <h1 id={rowData.uid + "-total_sold"} >{total_sold}</h1>
                  <b>Sold Listings</b>
                </div>
              </div>
            </div>
            <div className="col-md-2">
              <div className="row alert alert-warning ">
                <div className="col-md-6 text-right">
                  <h1 className="badger"><i className="fa fa-globe"></i></h1>
                </div>
                <div className="col-md-6 text-left">
                  <h1 id={rowData.uid + "-web_leads"}>{web_leads}</h1>
                  <b>Web Leads</b>
                </div>
              </div>
            </div>
            <div className="col-md-2">
              <div className="row alert alert-danger ">
                <div className="col-md-6 text-right">
                  <h1 className="badger"><i className="fa fa-street-view"></i></h1>
                </div>
                <div className="col-md-6 text-left">
                  <h1 id={rowData.uid + "-flex_leads"}>{flex_leads}</h1>
                  <b>Flex Leads</b>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="panel panel-info">
                <div className="panel-heading">
                  Domain Information
                    </div>
                <div className="panel-body">
                  <div className="col-md-6">
                    <b>Domain URL:</b>
                    <p><a target='_blank' href={'http://'+rowData.domains}>{rowData.domains}</a></p>
                  </div>
                  <div className="col-md-6">
                    <b>Subdomain URL:</b>
                    <p><a href="javascript:void(0)" type="button" onClick={ () => { this.redirectLink(rowData.subdomain_url) }}>{rowData.subdomain_url}</a></p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="panel panel-info">
                <div className="panel-heading">
                  Product Information
                    </div>
                <div className="panel-body">
                  <div className="col-md-4">
                    <b>Product:</b>
                    <p>{rowData.ApplicationType}</p>
                  </div>
                  <div className="col-md-4">
                    <b>Recent Sold Listings:</b>
                    <p id={rowData.uid + "-recent_sold_listings"}>{recent_sold_listings}</p>
                  </div>
                  <div className="col-md-4">
                    <b>Recent Active Listings:</b>
                    <p id={rowData.uid + "-recent_active_listings"}>{recent_active_listings}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="panel panel-info">
                <div className="panel-heading">
                  Payment Details
                    </div>
                <div className="panel-body">
                  <div className="col-md-6">
                    <b>Paying Customer:</b>
                    <p id={rowData.uid+"-paying_customer"}>{rowData.paid_amount ? <span className="label label-success">YES</span> : <span className="label label-danger">No</span>}</p>
                  </div>
                  <div className="col-md-6">
                    <b>Last Payment Date in Stripe:</b>
                    <p>{rowData.paid_amount ? payment_date_utc + "(System)" : "Not Applicable"}</p>

                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="panel panel-info">
                <div className="panel-heading">
                  Agent Information
                    </div>
                <div className="panel-body">
                  <div className="col-md-6">
                    <b>User ID:</b>
                    <p>{rowData.uid}</p>
                  </div>
                  <div className="col-md-6">
                    <b>Agent ID:</b>
                    <p>{rowData.agent_id}</p>
                  </div>
                  <div className="col-md-6">
                    <b>MLS ID:</b>
                    <p>{rowData.mls_id}</p>
                  </div>
                  <div className="col-md-6">
                    <b>MLS Name:</b>
                    <p>{rowData.mls_name}</p>
                  </div>
                  <div className="col-md-6">
                    <b>Company Name:</b>
                    <p>{rowData.company}</p>
                  </div>
                  <div className="col-md-6">
                    <b>Signup Date:</b>
                    <p>{rowData.date_signed ? rowData.date_signed : ''}</p>
                  </div>
                  <div className="col-md-6">
                    <b>Access Token:</b>
                    <p>{rowData.access_token}</p>
                  </div>
                  <div className="col-md-6">
                    <b>Bearer Token:</b>
                    <p>{rowData.bearer_token ? rowData.bearer_token : ''}</p>
                  </div>
                  <div className="col-md-6">
                    <b>Token Status:</b>
                    <p>{rowData.is_valid || rowData.bearer_token ? <span className="label label-success">LIVE</span> : <span className="label label-danger">LOST</span>}</p>
                  </div>
                </div>

              </div>
            </div>
            <div className="col-md-4">
              <div className="panel panel-info">
                <div className="panel-heading">
                  User Subscription
                    </div>
                <div className="panel-body">
                  <div className="col-md-6">
                    <b>Plan: </b>
                    <p>
                      <FreemiumPlanFormat 
                        row={
                          {
                            'uid': rowData.uid,
                            'paid_occurence': rowData.paid_occurence,
                            'paid_amount': rowData.paid_amount,
                            'PricingPlan': rowData.PricingPlan,
                            'IsFromSpark': rowData.IsFromSpark,
                            'mls_id': rowData.mls_id,
                            'freemium_plan_id': rowData.freemium_plan_id,
                            'trial': rowData.trial,
                            'IsFromSpark': rowData.IsFromSpark
                          }
                        }
                      />
                    </p>
                  </div>
                  <div className="col-md-6">
                    <b>Plan Status:</b>
                    <p>{rowData.trial ? "TRIAL" : "ACTIVE"}</p>
                  </div>
                  <div className="col-md-6">
                    <b>Users Subscription:</b>
                    <p id={`subs-status-${rowData.uid}`}>{rowData.EventType.charAt(0).toUpperCase() + rowData.EventType.slice(1)}</p>
                  </div>
                  <div className="col-md-6">
                    <b>Subscribed Via:</b>
                    <p>{rowData.IsFromSpark ? "Spark" : "Agentsquared"}</p>
                  </div>
                  <div className="col-md-12">
                    <b>Join Date (Date Agent received license):</b>
                    <p>{rowData.date_signed ? dateFormat(rowData.date_signed, "mmmm d, yyyy") : ""}</p>
                  </div>
                </div>
              </div>
              <div className="panel panel-info">
                <div className="panel-heading">
                  Social Media Details
                    </div>
                <div className="panel-body">
                  <div className="col-md-12">
                    <b>Facebook Link:</b>
                    <p id={rowData.uid + "-facebook"} > {rowData.facebook ? rowData.facebook : ''}</p>
                  </div>
                  <div className="col-md-12">
                    <b>Google Business Page Link:</b>
                    <p id={rowData.uid + "-googleplus"}>{rowData.googleplus ? rowData.googleplus : ''}</p>
                  </div>
                  <div className="col-md-12">
                    <b>LinkedIn Link::</b>
                    <p id={rowData.uid + "-linkedin"}>{rowData.linkedin ? rowData.linkedin : ''}</p>
                  </div>
                  <div className="col-md-12">
                    <b>Twitter Link:</b>
                    <p id={rowData.uid + "-twitter"}>{rowData.twitter ? rowData.twitter : ''}</p>
                  </div>
                </div>
              </div>
            </div>

            <LeadAssignment 
              rowData = {
                {
                  'uid': rowData.uid,
                  'lead_owner': rowData.lead_owner,
                  'lead_note': rowData.lead_note ? rowData.lead_note : '',
                  'sales_username': rowData.sales_username
                }
              }
              handlestatechanges = { updateState.bind(this) }
              sales_agents = { this.state.sales_agents }
            />
          </div>
          <DeleteCancelAgent 
            rowData={
              {
                'uid': rowData.uid,
                'full_name': rowData.full_name,
                'agent_id': rowData.agent_id,
                'freemium_plan_id': rowData.freemium_plan_id,
                'isPaid': rowData.receipt_id ? true : false
              }
            }
          />
        </div>
      </div>
    )
  }
  expandColumnComponent({ isExpandableRow, isExpanded }) {

    let content = '';

    if (isExpandableRow) {
      content = (isExpanded ? <i className="fa fa-angle-up"></i> : <i className="fa fa-angle-down"></i>);
    } else {
      content = ' ';
    }

    return (
      <div> <button className="btn btn-primary btn-xs"><b> {content}</b> </button> </div>
    );
  }

  onExportToCSV(row) {
    // ...
    return this.state.data;
  }

  handleShowNote() {
    this.setState({
      showNote : true
    })
  }

  handleHideNote() {
    this.setState({
      showNote : false
    })
  }

  lead(cell, row) {
    return (
      <p>Paul Rosario</p>
    )
  }
  notes(cell, row) {

    if(!cell)
      return null;

    let note = cell;
    let count = note.length;
    let _note = note.slice(0, 80);

    if (count < 80 ) {
      return note
    } else {
      return (
        <div>
          {_note} <a href="javascript:void(0)" onClick={this.handleShowNote.bind(this)} >Read more</a>

          <Modal show={this.state.showNote} onHide={this.handleHideNote} className="modal_hive modal-dashboard">
            <Modal.Header closeButton>Note</Modal.Header>
            <Modal.Body>
              <p>{note}</p>
            </Modal.Body>
          </Modal>
        </div>
      )
    }

  }


  renderTable(data) {

    const pricingPlan = this.state.PricingPlan;
    const applicationType = this.state.ApplicationType;
    const source = this.state.isFromSpark;
    const eventType = this.state.EventType;
    const isAdmin = (this.state.role === "admin" || this.state.role === "tech") ? true : false;
    const isSales = (this.state.role === "sales") ? true : false;
    var token, subscription, plan, product, syncCell, leadCreated, domain = null;

    const options = {
      onExportToCSV: this.onExportToCSV.bind(this),
      sizePerPage: this.state.limit,
      sizePerPageList: [ 20, 40, 100, 500, 1000 ],
      onSizePerPageList: this.onSizePerPageList.bind(this),
      onPageChange: this.onPageChange.bind(this),
      onSortChange: this.onSortChange.bind(this),
      onSearchChange: this.onSearchChange.bind(this),
      searchField: this.createCustomSearchField.bind(this),
      clearSearch: true,
      clearSearchBtn: this.createCustomClearButton.bind(this),
      paginationLoop: true,
      page: this.state.page,
      searchDelayTime: 1500,
      pageStartIndex: 1,
      paginationSize: 5,
      prePage: 'Prev',
      nextPage: 'Next',
      firstPage: 'First',
      lastPage: 'Last',
      paginationShowsTotal: true,
      // hideSizePerPage: true,
      noDataText: this._setTableOption(),
      toolBar: this.createCustomToolBar,
      expandBy: 'column',
      expandParentClass: function (row, rowIndex) {
        return 'expand-parent-detail-' + row.uid;
      },
      expandBodyClass: function (row, rowIndex, isExpanding) {

        if (isExpanding) {

          const params = {
            'user_id' : row.uid,
            'agent_id' : row.agent_id
          };

          console.log("Expandingz");
          axios.post(
            `${hiveConfig.ace.apiURL}get-agent-details`, params
          ).then(function(e) {
            // e.data[0]
            if (e.data.length) {
              const rowData = e.data[0];
              const recent_sold_listings = (rowData.recent_sold_date !== null && rowData.recent_sold_date !== '0000-00-00' && rowData.recent_sold_date !== '0000-00-00 00:00:00') ? dateFormat(rowData.recent_sold_date, "mmmm d, yyyy") : "Not Applicable";
              const recent_active_listings = (rowData.recent_active_date !== null && rowData.recent_active_date !== '0000-00-00' && rowData.recent_active_date !== '0000-00-00 00:00:00') ? dateFormat(rowData.recent_active_date, "mmmm d, yyyy") : "Not Applicable";

              $("#" + row.uid + "-total_active").html(rowData.total_active || '0');
              $("#" + row.uid + "-total_sold").html(rowData.total_sold || '0');
              $("#" + row.uid + "-web_leads").html(rowData.web_leads || '0');
              $("#" + row.uid + "-flex_leads").html(rowData.flex_leads || '0');

              $("#" + row.uid + "-recent_sold_listings").html(recent_sold_listings);
              $("#" + row.uid + "-recent_active_listings").html(recent_active_listings);

              $("#" + row.uid + "-facebook").html(rowData.facebook);
              $("#" + row.uid + "-googleplus").html(rowData.googleplus);
              $("#" + row.uid + "-linkedin").html(rowData.linkedin);
              $("#" + row.uid + "-twitter").html(rowData.twitter);
            }
          }).catch(function(e) {
            throw(e);
          });

        }
        return 'detail-' + row.uid;
      }
    };

    $.each(pricingPlan, function (i, item) {
      if(item.PricingPlan) {
        $('#productOption_plan').append($('<option>', {
            value: item.PricingPlan,
            text : item.PricingPlan
        }));
      }

    });

    $.each(source, function (i, item) {
        $('#productOption_source').append($('<option>', {
            value: item.isFromSpark,
            text : item.isFromSpark ? "Spark" : "AgentSquared"
        }));
    });

    $.each(applicationType, function (i, item) {
      if(item.ApplicationType) {
        $('#productOption_product').append($('<option>', {
            value: item.ApplicationType,
            text : item.ApplicationType.replace(/_/g, ' ')
        }));
      }

    });

    $.each(eventType, function (i, item) {
      // console.log(item);
      if(item.EventType) {
        $('#productOption_purchaseStatus').append($('<option>', {
            value: item.EventType,
            text : item.EventType.replace(/_/g, ' ')
        }));
      }

    });

    // const tokenStatus = {
    //   0: this.lostToken.bind(this),
    //   1: 'LIVE'
    // };
    const tokenStatusCSV = {
      0: 'LOST',
      1: 'LIVE'
    };
    const purchasedCancelledType = {
      'purchased': 'Purchased',
      'canceled': 'Cancelled'
    }
    const domainType = {
      0 : 'Domain',
      1 : 'Sub-Domain'
    }

    if (isAdmin) {
      syncCell = <TableHeaderColumn width='50' dataField='button' dataAlign='center' dataFormat={this.sync_status} export={false} >ReSync</TableHeaderColumn>;
    }

    if (!isSales) {

      domain = <TableHeaderColumn width='170' expandable={false} dataField='domains' dataFormat={this.domainLink} csvHeader="Domain" csvFormat={ this.csvCellFormatter } dataSort={true}>Domain</TableHeaderColumn>
      plan = <TableHeaderColumn width='150' expandable={false} dataField='PricingPlan' csvHeader="Plan" csvFormat={this.freemiumPlanFormatter } dataFormat={ this.freemiumPlanFormatter }  dataSort={true}>Plan</TableHeaderColumn>
      product = <TableHeaderColumn width='75' expandable={false} dataField='ApplicationType' dataFormat={this.productNameFormat} csvFormat={ this.productNameFormatExport } csvHeader="Product"  dataSort={false}>Product</TableHeaderColumn>
      subscription = <TableHeaderColumn width='150' expandable={false} dataAlign='center' dataField='EventType' dataFormat={this.enumPurchaseFormatter} formatExtraData={purchasedCancelledType} dataSort={true} csvFormat={this.enumPurchaseFormatter } csvFormatExtraData={purchasedCancelledType} csvHeader="Purchased/Cancelled">Subscription</TableHeaderColumn>;
      token = <TableHeaderColumn width='105' expandable={false} dataAlign='center' dataField='is_valid' dataFormat={ this.tokenStatusFormat } csvFormatExtraData={tokenStatusCSV} dataSort={true} csvFormat={ this.enumFormatter } csvHeader="Token Status">Token Status</TableHeaderColumn>;

    } else {

      leadCreated = <TableHeaderColumn width='150' expandable={false} expand={false} dataField="signup_date" dataAlign="left" dataFormat={this.dateFormatter}>Lead Added</TableHeaderColumn>

    }
              
              
    return (

      <div className="table-data">
        {this.fetchingDataLoader()}
        <BootstrapTable
            data={data}
            striped
            hover
            width="100%"
            scrollTop={ 'Bottom' }
            className="hive_table "
            bordered={false}
            pagination={true}
            options={options}
            search={ true }
            exportCSV
            remote={ true }
            fetchInfo={ { dataTotalSize: this.state.total } }
            csvFileName='command-center-agents.csv'
            expandableRow={() => { return true; }}
            expandComponent={this.expandComponent.bind(this)}
            expandColumnOptions={{
              expandColumnVisible: true,
              expandColumnComponent: this.expandColumnComponent
            }}
            >
            <TableHeaderColumn width='55'  expandable={true} dataField='uid' csvHeader="User ID" dataSort={true} isKey>User ID</TableHeaderColumn>
            <TableHeaderColumn width='150' expandable={false} dataField='full_name' csvHeader="Name" dataSort={true}>Name</TableHeaderColumn>
            <TableHeaderColumn width='200' expandable={false} dataField='email' dataFormat={this.emailLink}  csvHeader="Email"  dataSort={true}>Email</TableHeaderColumn>
            { domain }
            <TableHeaderColumn  expandable={false} dataField="subdomain_url" export= {true } hidden csvHeader="Subdomain" csvFormat={ this.csvCellFormatter }  dataSort={true}>Sub Domain</TableHeaderColumn>
            <TableHeaderColumn width='100'  expandable={false} dataField='mls_name' csvHeader="MLS Name" csvFormat={ this.csvCellFormatter }  dataSort={true}>MLS Name</TableHeaderColumn>
            <TableHeaderColumn width='100' expandable={false} expand={false} dataField='mls_id' hidden export={true} csvHeader="MLS ID" csvFormat={ this.csvCellFormatter }></TableHeaderColumn>
            { plan }
            { product }
            <TableHeaderColumn width='120' expandable={false} dataField='phone'  dataSort={true} tdStyle={ { whiteSpace: 'normal' } } >Phone</TableHeaderColumn>
            {token }
            {subscription }
            {syncCell }
            { leadCreated }
            <TableHeaderColumn width='100' dataField='sales_username' export={true} dataAlign='center' >Lead Owner</TableHeaderColumn>
            <TableHeaderColumn width='200' dataField='lead_note' export={true} dataAlign='center' dataFormat={this.notes.bind(this)} export={false} tdStyle={ { whiteSpace: 'normal' } } >Notes</TableHeaderColumn>
            <TableHeaderColumn width='100' expandable={false} dataField="button" dataFormat={this.actionButtonFormatter} export={false}> Action </TableHeaderColumn>

            <TableHeaderColumn dataField="num_leads_flex" export= {true } hidden csvHeader="Flex Leads" csvFormat={ this.csvCellFormatter } >Flex Leads</TableHeaderColumn>
            <TableHeaderColumn dataField="num_leads_web" export= {true } hidden csvHeader="Web Leads" csvFormat={ this.csvCellFormatter }>Web Leads</TableHeaderColumn>
            <TableHeaderColumn dataField="signup_date" dataAlign="left" hidden export={true} csvHeader="Registration Date" csvFormat={ this.csvCellFormatter }>Registration Date</TableHeaderColumn>
            <TableHeaderColumn dataField="is_subdomain" formatExtraData={ domainType } csvFormat={ this.enumFormatter } csvFormatExtraData={domainType} export= {true } hidden csvHeader="Website Type">Website Type</TableHeaderColumn>

            <TableHeaderColumn dataField="facebook" export= {true } hidden csvHeader="Facebook" csvFormat={ this.csvCellFormatter }>Facebook</TableHeaderColumn>
            <TableHeaderColumn dataField="twitter" export= {true } hidden csvHeader="Twitter" csvFormat={ this.csvCellFormatter }>Twitter</TableHeaderColumn>
            <TableHeaderColumn dataField="linkedin" export= {true } hidden csvHeader="LinkedIn" csvFormat={ this.csvCellFormatter }>LinkedIn</TableHeaderColumn>
            <TableHeaderColumn dataField="googleplus" export= {true } hidden csvHeader="Google Plus" csvFormat={ this.csvCellFormatter }>Google Plus</TableHeaderColumn>

            <TableHeaderColumn dataField="last_payment_date" export= {true } hidden csvHeader='Last Payment Date' csvFormat={ this.csvCellFormatter }>Last Payment Date</TableHeaderColumn>
            <TableHeaderColumn dataField="freemium_signup_date" export= {true } hidden csvHeader='Freemium Signup Date' csvFormat={ this.csvCellFormatter }>Freemium Signup Date</TableHeaderColumn>
            <TableHeaderColumn dataField="paid_date" export={true} hidden csvHeader='Date upgraded to Pro' csvFormat={this.csvCellFormatter}>Date Upgraded to PRO</TableHeaderColumn>
            <TableHeaderColumn dataField="cancel_date" export= {true } hidden csvHeader='Cancellation Date' csvFormat={ this.csvCellFormatter }>Cancellation Date</TableHeaderColumn>
            <TableHeaderColumn dataField="total_sold" export= {true } hidden csvHeader='Total Properties Sold' csvFormat={ this.csvCellFormatter }>Total Properties Sold</TableHeaderColumn>
            <TableHeaderColumn dataField="total_active" export= {true } hidden csvHeader='Total Active Properties' csvFormat={ this.csvCellFormatter }>Total Active Properties</TableHeaderColumn>
            <TableHeaderColumn dataField="recent_sold_date" export= {true } hidden csvHeader='Recent Sold Property Date' csvFormat={ this.csvCellFormatter }>Recent Sold Date</TableHeaderColumn>
            <TableHeaderColumn dataField="recent_active_date" export= {true } hidden csvHeader='Recent Active Property Date' csvFormat={ this.csvCellFormatter }>Recent Active Property Date</TableHeaderColumn>
            {/* paid_date */}
        </BootstrapTable>
      </div>
    );
  }

  render() {
    return (
      <div className="container body">
        <div className="main_container">
          <Sidebar />
          <TopNav />
          <div className="right_col" role="main">
            <div className="col-md-12">
              <PageTitle title={title}/>
            </div>
            <div className="col-md-12">
              {this.renderTable(this.state.data)}
            </div>
          </div>
          <Footer />
        </div>
      </div>
    )
  }
}
