module.exports = {
  ace : {
      apiURL: process.env.REACT_APP_ACE_API_URL
  },
  certProcUrl: process.env.REACT_APP_CERT_PROC_URL
};
