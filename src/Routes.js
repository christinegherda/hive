import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Login from './components/Login.js';
import Agent from './page/agent/Agent.js';
import AgentExport from './page/agent/Export';
import Domain from './page/domain/Domain.js';
import SPW from './page/spw/SPW.js';
import CaptureLeads from './page/captureleads/CaptureLeads.js';
import Users from './page/users/Users.js';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/stackslide.css';

const auth = require('./session/auth.js');
// console.log(auth);
const role = auth.getCookie('role');

const Routes = () => (

  <div>
    <Router>
      <div>
        <Route exact path="/" component={Login} />
        <Route exact path="/agent" component={Agent} />
        <Route exact path="/agent-export" component={AgentExport} />
        <Route exact path="/domains" component={Domain} />
        <Route exact path="/spw" component={SPW} />
        {(role !== 'sales' && role !== 'tech') ? <Route exact path="/capture-leads" component={CaptureLeads} /> : ''}
        {(role !== 'sales' && role !== 'tech') ? <Route exact path="/users" component={Users} /> : ''}
      </div>
    </Router>
  </div>
);


export default Routes;
