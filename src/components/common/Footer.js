import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
		<footer>
          <div className="pull-right">
            HIVE by <a href="https://agentsquared.com/">Agentsquared</a>
          </div>
          <div className="clearfix"></div>
        </footer>
    );
  }
}

export default Footer;