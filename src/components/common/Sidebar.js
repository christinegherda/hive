import React  from 'react';
import { Link } from "react-router-dom";
import brand from '../../assets/images/default-favicon.png';
import $ from 'jquery';
const auth = require('../../session/auth.js');


class Sidebar extends React.Component {
  componentDidMount() {
    let CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
        $BODY = $('body'),
        $MENU_TOGGLE = $('#menu_toggle'),
        $SIDEBAR_MENU = $('#sidebar-menu'),
        // $SIDEBAR_FOOTER = $('.sidebar-footer'),
        $LEFT_COL = $('.left_col'),
        $RIGHT_COL = $('.right_col');
        // $FOOTER = $('footer');
        $BODY.addClass("nav-sm");

        var setContentHeight = function () {

          $RIGHT_COL.css('min-height', $(document).height());
          $LEFT_COL.css('min-height', $(document).height());
        };

        $SIDEBAR_MENU.find('a').on('click', function(ev) {
          console.log('clicked - sidebar_menu');
              var $li = $(this).parent();

              if ($li.is('.active')) {
                  $li.removeClass('active active-sm');
                  $('ul:first', $li).slideUp(function() {
                      setContentHeight();
                  });
              } else {
                  // prevent closing menu if we are on child menu
                  if (!$li.parent().is('.child_menu')) {
                      $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                      $SIDEBAR_MENU.find('li ul').slideUp();
                  }else
                  {
              if ( $BODY.is( ".nav-sm" ) )
              {
                $SIDEBAR_MENU.find( "li" ).removeClass( "active active-sm" );
                $SIDEBAR_MENU.find( "li ul" ).slideUp();
              }
            }
                  $li.addClass('active');

                  $('ul:first', $li).slideDown(function() {
                      setContentHeight();
                  });
              }
        });

        // toggle small or large menu
        $MENU_TOGGLE.on('click', function() {
            if ($BODY.hasClass('nav-md')) {
                $SIDEBAR_MENU.find('li.active ul').hide();
                $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
            } else {
                $SIDEBAR_MENU.find('li.active-sm ul').show();
                $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
            }

            $BODY.toggleClass('nav-md nav-sm');

            setContentHeight();

            $('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
        });

        // check active menu
        $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

        $SIDEBAR_MENU.find('a').filter(function () {
            return this.href === CURRENT_URL;
        }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
            setContentHeight();
        }).parent().addClass('active');

        // recompute content when resizing
        // $(window).smartresize(function(){
        //     setContentHeight();
        // });

        setContentHeight();

        // fixed sidebar
        if ($.fn.mCustomScrollbar) {
            $('.menu_fixed').mCustomScrollbar({
                autoHideScrollbar: true,
                theme: 'minimal',
                mouseWheel:{ preventDefault: true }
            });
        }

  }

  render() {
    const role = auth.getCookie('role');
    return (
        <div className="col-md-3 left_col">
          <div className="left_col scroll-view">
            <div className="navbar nav_title" style={{border: 0}}>
              <Link to="/" className="site_title">
                <img src={brand} alt=""></img>
                <span>HIVE</span>
              </Link>
            </div>
            <div className="clearfix"></div>
            <div id="sidebar-menu" className="main_menu_side hidden-print main_menu">
              <div className="menu_section">
                <ul className="nav side-menu">
                  <li>
                    <Link to="/agent"><i className="fa fa-user"></i> Agents</Link>
                  </li>
                  <li>
                    <Link to="/domains"><i className="fa fa-globe"></i> Domains</Link>
                  </li>
                  <li>
                    <Link to="/spw"><i className="fa fa-desktop"></i> SPW</Link>
                  </li>
                  <li>
                    {(role !== 'sales' && role !== 'tech') ? <Link to="/capture-leads"><i className="fa fa-universal-access"></i> Capture Leads</Link> : '' }
                  </li>
                  <li>
                    {(role !== 'sales' && role !== 'tech') ? <Link to="/users"><i className="fa fa-users"></i> Users</Link> : '' }
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default Sidebar;
