import React, { Component } from 'react';
const auth = require('../../session/auth.js');

class Topnav extends Component {
  logout() {
    console.log("Logout");
    auth.logout();
  }

  render() {
    return (
        <div className="top_nav">
          <div className="nav_menu">
            <nav>
              <div className="nav toggle">
                <a id="menu_toggle"><i className="fa fa-bars"></i></a>
              </div>

              <ul className="nav navbar-nav navbar-right">
                <li className="">
                  <a className="user-profile " href="javascript:void(0)" onClick={this.logout}>
                    Log Out
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
    );
  }
}

export default Topnav;
