import React from "react";

export default class Agent extends React.Component {
	render() {
		let pagetitle = this.props;
		let subtitle = '';
		
		if (pagetitle.title[1]) {
			subtitle = pagetitle.title[1];
		} 
		return (
		    <div className="page-title">
		        <div className="title_left">
		          <h3>{pagetitle.title[0]} </h3>
		          <p>{subtitle}</p>
		        </div>
		    </div>
		)
	}
}