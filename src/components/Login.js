import React  from 'react';
import { Link } from "react-router-dom";
import axios from 'axios'
import $ from 'jquery';

const auth = require('../session/auth.js');

const hiveConfig = require('../hive.config.js');

class Login extends React.Component {
  constructor(props) {
    super(props);
    // auth.logout();
    const authStatus = auth.checkAuth().then(function(e) {
      console.log(e);
      if(e) {
        // True
        window.location = "/agent";
      }
      else {
        // False, continue
        console.log("Un authenticated, please log in");
      }
    });
  }

  triggerLogin(e) {
    e.preventDefault();
    console.log("Login");
    /* Validate Form */
    const email = $("#TriggerLogin .email").val();
    const pass = $("#TriggerLogin .password").val();
    if(!email && !pass) {
      var errorFields = ` ${email ? '' : 'Email'} ${pass ? '' : 'Password'} is required`;
      if(!email) {
        $("#TriggerLogin .email-group").addClass("has-error");
      }
      if(!pass) {
        $("#TriggerLogin .pass-group").addClass("has-error");
      }
      $("#errorField").addClass("alert alert-danger").html(errorFields);
    }
    else {
      if(email) {
        $("#TriggerLogin .email-group").removeClass("has-error");
      }
      if(pass) {
        $("#TriggerLogin .pass-group").removeClass("has-error");
      }
    }
    if(email && pass) {
      $("#errorField").removeClass("alert alert-danger").html('');
      $("#errorField").addClass("alert alert-success").html(`Logging in please wait... <i class="fa fa-gear fa-spin"></i>`);

      /* Trigger Login */
      const params = {
        'username' : email,
        'password' : pass
      }
      axios.post(`${hiveConfig.ace.apiURL}user-login`, params).then(function (response) {
        /*
          - Set session / cookie
          - Redirect to dashboard
        */
        if(response.data.length) {
          console.log("Login Success");
          $("#errorField").addClass("alert alert-success").html(`<i class="fa fa-check"></i> Success! Redirecting please wait.`);
          // Generate UUID and set the session
          const session = (Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)) +
                          (Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)) + '-' +
                          (Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)) + '-' +
                          (Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)) + '-' +
                          (Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)) + '-' +
                          (Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)) +
                          (Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)) +
                          (Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1));
          const id = response.data[0].id;
          const role = response.data[0].role;
          console.log(session);
          console.log(id);
          const sessionParams = {
            'session': session,
            'id' : id,
            'role' : role
          }
          axios.post(`${hiveConfig.ace.apiURL}save-usersession`, sessionParams).then(function (response) {
            // Redirect User
            document.cookie = `id=${id}`;
            document.cookie = `session=${session}`;
            document.cookie = `role=${role}`;
            console.log(document.cookie);
            window.location = "/agent";
          }).catch(function(e) {
            console.log(e);
          });
        }
        else {
          // Invalid Login
          console.log("Invalid Login");
          $("#errorField").addClass("alert alert-danger").html(`<i class="fa fa-exclamation-triangle"></i> Invalid Login`);
        }
      }).catch(function (error) {
        console.log(error);
      });
    }

  }

  render() {
    return (
      <div className="section-login-bg">
        <div className="login-wrapper">
          <form id="TriggerLogin" className="login" method="post">
          <div className="top-bg-house"></div>
            <h3>Sign in to Hive </h3>
            <p id="errorField"></p>
            <div className="form-group email-group">
              <input type="email" className="form-control email" placeholder="Email" name="email" required/>
            </div>
            <div className="form-group pass-group">
              <input type="password" className="form-control password" placeholder="Password" name="password" required />
            </div>
            <div className="form-group">
              <Link to="/reset_pass">Reset password</Link> <br /><br />
              <button className="button btn-blue" onClick={this.triggerLogin}>Login</button>
            </div>
            <div className="bottom-bg-house"></div>
          </form>
          <br />
          <div className="text-center">
            <p>©2018 All Rights Reserved. Agentsquared</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
