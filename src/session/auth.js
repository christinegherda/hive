import axios from 'axios'
import $ from 'jquery';

const hiveConfig = require('../hive.config.js');

function Auth(){
  console.log("constructor");
}

export function getCookie (cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
  return "";
}

export function logout() {
  const idCookie = getCookie('id');
  const sessionCookie = getCookie('session');
  const params = {
    'id': idCookie,
    'session' : ''
  };
  document.cookie = `id=`;
  document.cookie = `session=`;
  axios.post(`${hiveConfig.ace.apiURL}save-usersession`, params).then(function (response) {
    console.log(response);
    window.location = "/";
  }).catch(function(e) {
    console.log(e);
  });
}

export function checkAuth () {
  // console.log("Auth");
  const idCookie = getCookie('id');
  const sessionCookie = getCookie('session');
  const roleCookie = getCookie('role');
  console.log("Logged in as : ", idCookie);
  console.log("With session : ", sessionCookie);
  console.log("Role : ", roleCookie);
  // Check session in DB
  const params = {
    'id': idCookie,
    'session' : sessionCookie
  };
  return axios.post(`${hiveConfig.ace.apiURL}check-usersession`, params).then(function (response) {
    console.log("Check auth data");
    console.log(response.data);
    if(response.data.length) {
      // Logged In, redirect
      return true;
    }
    else {
      // Not Logged In
      return false;
    }
  }).catch(function(e) {
    console.log(e);
  });
}

export function checkAuthUser () {
  console.log("Auth");
  const idCookie = getCookie('id');
  const sessionCookie = getCookie('session');
  console.log("Logged in as : ", idCookie);
  console.log("With session : ", sessionCookie);
  // Check session in DB
  const params = {
    'id': idCookie,
    'session' : sessionCookie
  };
  return axios.post(`${hiveConfig.ace.apiURL}check-usersession`, params).then(function (response) {
    console.log(response.data);
    if(response.data.length) {
      // Logged In, redirect
      return response.data;
    }
    else {
      // Not Logged In
      return false;
    }
  }).catch(function(e) {
    console.log(e);
  });
}
