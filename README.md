# Hive

![Build Status](https://codebuild.us-west-2.amazonaws.com/badges?uuid=eyJlbmNyeXB0ZWREYXRhIjoiN3NkSHNwa2RuWnlwREpScXRUd3VERUg3VUxwZllqVnJzVVZaQkswVjh4ZWZMVnllVC9VZWlzSVRDNjdseVczckFLc2N2cGRwV1hXam5HYU9sOTB6MFRBPSIsIml2UGFyYW1ldGVyU3BlYyI6InY2Vi9ZM3dHK241ck1SWE8iLCJtYXRlcmlhbFNldFNlcmlhbCI6MX0%3D&branch=master)

AgentSquared admin panel and internal tools

### Architecture

See [here](https://agentsquared.atlassian.net/wiki/spaces/DEV/pages/244023449/AWS+Architecture+2018+Roadmap)

