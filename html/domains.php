<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/default-favicon.png">
    
    <title>Domains </title>

    <!-- Bootstrap -->
    <link href="assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/vendors/font-awesome/css/fontawesome-all.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="assets/css/main.css" rel="stylesheet">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
          
        <?php include('common/navbar.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="col-md-12">
            <div class="page-title">
              <div class="title_left">
                <div class="col-md-5">
                  <h3>Domains</h3>
                  <p>Showing 1-10  of 2000</p>
                </div>
              </div>

              <!-- Search Form -->
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-striped jambo_table table-l1">
                  <thead>
                    <tr class="headings">
                      <th class="column-title"><a href="#">No. <i class="fa fa-caret-down"></i></a> </th>
                      <th class="column-title"><a href="#">Domain <i class="fa fa-caret-down"></i></a> </th>
                      <th class="column-title"><a href="#">Subdomain  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">Name  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">SSL  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">Status  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title no-link last" width="110"><span class="nobr">Action</span>
                      </th>
                      <th class="bulk-actions" colspan="7">
                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                      <tr class="collapseable collapsed">
                        <td class="">1</td>
                        <td class=""><a href="http://claudialockwood.agentsquared.com" target="_blank">http://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="http://claudialockwood.agentsquared.com" target="_blank">http://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-danger">Not Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-warning">Pending</span>
                        </td>
                        <td>
                          
                        </td>
                      </tr>
                      <tr class="collapseable collapsed">
                        <td class="">2</td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-success">Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-success">Completed</span>
                        </td>
                        <td>
                          <button class="button btn-green"><i class="fa fa-check"></i> Email Sent</button>
                        </td>
                      </tr>
                      <tr class="collapseable collapsed">
                        <td class="">3</td>
                        <td class=""><a href="http://claudialockwood.agentsquared.com" target="_blank">http://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="http://claudialockwood.agentsquared.com" target="_blank">http://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-danger">Not Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-warning">Pending</span>
                        </td>
                        <td>
                          
                        </td>
                      </tr>
                      <tr class="collapseable collapsed">
                        <td class="">4</td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-success">Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-success">Completed</span>
                        </td>
                        <td>
                          <button class="button btn-blue"><i class="fa fa-paper-plane"></i> Send Email</button>
                        </td>
                      </tr>
                      <tr class="collapseable collapsed">
                        <td class="">5</td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-danger">Not Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-success">Completed</span>
                        </td>
                        <td>
                          <button class="button btn-blue"><i class="fa fa-paper-plane"></i> Send Email</button>
                        </td>
                      </tr>
                      <tr class="collapseable collapsed">
                        <td class="">6</td>
                        <td class=""><a href="http://claudialockwood.agentsquared.com" target="_blank">http://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="http://claudialockwood.agentsquared.com" target="_blank">http://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-danger">Not Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-warning">Pending</span>
                        </td>
                        <td>
                          
                        </td>
                      </tr>
                      <tr class="collapseable collapsed">
                        <td class="">7</td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-success">Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-success">Completed</span>
                        </td>
                        <td>
                          <button class="button btn-green"><i class="fa fa-check"></i> Email Sent</button>
                        </td>
                      </tr>
                      <tr class="collapseable collapsed">
                        <td class="">8</td>
                        <td class=""><a href="http://claudialockwood.agentsquared.com" target="_blank">http://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="http://claudialockwood.agentsquared.com" target="_blank">http://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-danger">Not Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-warning">Pending</span>
                        </td>
                        <td>
                          
                        </td>
                      </tr>
                      <tr class="collapseable collapsed">
                        <td class="">9</td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-success">Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-success">Completed</span>
                        </td>
                        <td>
                          <button class="button btn-blue"><i class="fa fa-paper-plane"></i> Send Email</button>
                        </td>
                      </tr>
                      <tr class="collapseable collapsed">
                        <td class="">10</td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>
                        <td class=""><a href="https://claudialockwood.agentsquared.com" target="_blank">https://claudialockwood.agentsquared.com</a></td>  
                        <td class="">Claudia Lockwood</td>  
                        <td class="">
                          <span class="label label-danger">Not Secured</span>
                        </td> 
                        <td class="">
                        <span class="label label-success">Completed</span>
                        </td>
                        <td>
                          <button class="button btn-blue"><i class="fa fa-paper-plane"></i> Send Email</button>
                        </td>
                      </tr>
                  </tbody>
                </table>
              </div>
            </div>
            
          </div>
        </div>
        <!-- /page content -->

        <!-- modal delte -->
        <div class="modal modal-delete" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Modal body text goes here.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <!-- modal delte -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Hive by <a href="https://agentsquared.com/">Agentsquared</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="assets/js/main.js"></script>

  </body>
</html>