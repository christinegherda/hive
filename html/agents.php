<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/default-favicon.png">

    <title>Agents </title>

    <!-- Bootstrap -->
    <link href="assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/vendors/font-awesome/css/fontawesome-all.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="assets/css/main.css" rel="stylesheet">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
          
        <?php include('common/navbar.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="col-md-12">
            <div class="page-title">
              <div class="col-md-5">
				<div class="title_left">
                	<h3><span>Agents</span> <a href="#" class="button btn-green"><i class="fa fa-file"></i> Export Data</a></h3>
                	<p>Showing 1-20 of 2000 </p>
                	<p></p>
              	</div>
              </div>

			<!-- Search Form -->
              <div class="col-md-7 title_right">
	            <div class="col-md-8 col-sm-5 col-xs-12 form-group pull-right top_search">
					<form action="" class="form-horizontal">
		                  <label for="" class="col-md-2 mt-8">Sort By: </label>
		                  <div class="col-md-5">
		                  	<select name="" id="" class="form-control">
		                  	<option value="">Plan</option>
		                  	<option value="">15 Days Trial</option>
		                  	<option value="">Freemium</option>
		                  	<option value="">Premium</option>
		                  </select>
		                  </div>
		                  <div class="input-group">
		                    <input type="text" class="form-control" placeholder="Search for...">
		                    <span class="input-group-btn">
		                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
		                    </span>
		                  </div>
					</form>
	            </div>
              </div>
            </div>
            <div class="col-md-12">
				<div class="table-responsive">
	              <table class="table table-striped jambo_table table-l1 table-agents">
	                <thead>
	                  <tr class="headings">
	                    <th class="column-title text-center"><a href="#" class="text-underline">No</a> </th>
	                    <th class="column-title text-center">
	                    	<a href="#" class="text-underline">Name </a>
	                    </th>
	                    <th class="column-title text-center">
	                    	<a href="#" class="text-underline">Email </a>
	                    </th>
	                    <th class="column-title text-center">
	                    	<a href="#" class="text-underline">Domain </a>
	                    </th>
	                    <th class="column-title text-center dropdown">
	                    	<a href="#" data-toggle="dropdown" class="dropdown-toggle">MLS Name  <i class="fa fa-caret-down"></i></a>
		                  	<ul class="dropdown-menu">
		                      <li><a href="#">ARMLS</a></li>
		                      <li><a href="#">Michric</a></li>
		                      <li><a href="#">Sibor</a></li>
		                   	</ul>
	                    </th>
	                    <th class="column-title text-center dropdown">
	                    	<a href="#" data-toggle="dropdown" class="dropdown-toggle">Plan  <i class="fa fa-caret-down"></i></a>
		                  	<ul class="dropdown-menu">
		                      <li><a href="#">15 Days Trial</a></li>
		                      <li><a href="#">Freemium</a></li>
		                      <li><a href="#">Premium</a></li>
		                   	</ul>
	                    </th>
	                    <th class="column-title text-center dropdown">
	                    	<a href="#" data-toggle="dropdown" class="dropdown-toggle">Product  <i class="fa fa-caret-down"></i></a>
		                  	<ul class="dropdown-menu">
		                      <li><a href="#">Agent_IDX_Website</a></li>
		                      <li><a href="#">SPW</a></li>
		                   	</ul>
	                    </th>
	                    <th class="column-title text-center dropdown">
	                    	<a href="#" data-toggle="dropdown" class="dropdown-toggle">Purchased/Cancelled  <i class="fa fa-caret-down"></i></a>
		                  	<ul class="dropdown-menu">
		                      <li><a href="#">Purchased</a></li>
		                      <li><a href="#">Cancelled</a></li>
		                   	</ul>
	                    </th>
	                    <th class="column-title text-center dropdown">
	                    	<a href="#" data-toggle="dropdown" class="dropdown-toggle">Token Status  <i class="fa fa-caret-down"></i></a>
		                  	<ul class="dropdown-menu">
		                      <li><a href="#">LOST</a></li>
		                      <li><a href="#">LIVE</a></li>
		                   	</ul>
	                    </th>
	                    <!-- If super admin -->
	                    <th class="column-title text-center" width="100"><a href="#">Sync Status </a></th>
	                    <!-- If super admin -->
	                    <th class="column-title no-link last" width="70"><span class="nobr">Action</span> </th>
	                  </tr>
	                </thead>

	                <tbody>
	                <?php for ($i=1; $i < 21; $i++) { ?>
		                  <tr class="collapseable collapsed agent-row">
		                    <td class="text-center"><?php echo $i; ?></td>
		                    <td class="text-center">Duane Washkowiak</td>
		                    <td class="text-center"><a href="mailto:Office@PropertyinAZ.com">Office@PropertyinAZ.com</a></td>
		                    <td class="text-center"><a href="https://www.propertyinaz.com/">https://www.propertyinaz.com/</a></td> 
		                    <td class="text-center">ARMLS</td>  
		                    <td class="text-center">15 Days Trial</td>  
		                    <td class="text-center">Agent_IDX_Website</td> 
		                    <td class="text-center">Purchased</td>
		                    <td class="text-center">
		                    <?php if ($i % 2 == 0): ?>
		                    	LIVE
		                    <?php else: ?>
		                    	<a href="" class="button btn-red" > LOST </a>
		                    <?php endif ?></td>
		                    <td class="text-center">
		                      <!-- If super admin -->
		                      
		                    <?php if ($i % 2 == 0): ?>
		                    	<p class="text-center">Successful</p>
		                    <?php else: ?>
		                    	<a href="#" class="button btn-warning"> <i class="fa fa-sync"></i></a>
		                    <?php endif ?></td>
		                      <!-- If super admin -->
		                    </td>
		                    <td>
		                      	<a href="#" class="button btn-blue more-details modal-target_<?php echo $i; ?>" data-target=".modal-details_<?php echo $i; ?>" data-toggle="tooltip" data-placement="top" title="More Details"  data-id="<?php echo $i; ?>"><i class="fa fa-eye"></i></a>
		                      	<a href="https://dashboard.agentsquared.com/" class="button btn-green" target="_blank" data-toggle="tooltip" data-placement="top" title="Login to Dashboard"><i class="fa fa-sign-out-alt"></i></a>
		                    </td>
		                  </tr>
				            <!-- modal delte -->
				              <div class="modal fade modal_hive modal-delete delete_<?php echo $i; ?>" tabindex="-1" role="dialog" aria-hidden="true">
				                <div class="modal-dialog">
				                  <div class="modal-content">
				                    <div class="modal-header">
				                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				                      </button>
				                      <h4 class="modal-title" id="myModalLabel">Delete User</h4>
				                    </div>
				                      <form action="">
				                        <div class="modal-body">
				                          <p>Are you sure you want to delete Duane Washkowiak?</p>
				                        </div>
				                        <div class="modal-footer">
				                          <button class="button btn-grey" type="submit">Cancel</button>
				                          <button class="button btn-red" type="submit">Delete</button>
				                        </div>
				                      </form>
				                 </div>
				               </div>
				              </div>
				            <!-- modal delte -->

		                  <div class="modal fade modal-details_<?php echo $i; ?> modal_hive" tabindex="-1" role="dialog" aria-hidden="true">
		                    <div class="modal-dialog modal-lg">
		                      <div class="modal-content">

		                        <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
		                          </button>
		                          <h4 class="modal-title" id="myModalLabel">Agent Information</h4>
		                        </div>
		                        <div class="modal-body">
									<div class="row">
			                          <div class="col-md-3">
			                          	   <div class="row">
				                          	   	<div class="col-md-5">
					                          		<div class="agent-image">
					                          			<img src="assets/images/img.jpg" alt="" class="img-circle">
					                          		</div>
				                          	   	</div>
				                          	   	<div class="col-md-7">
					                          		<p class="agent-name">Duane Washkowiak</p>
					                          		<a href="tel:480-313-2222" class="agent-phone">480-313-2222</a> <br>
					                          		<a href="mailto:Office@PropertyinAZ.com" class="agent-email">Office@PropertyinAZ.com</a>
				                          	   	</div>
			                          	   </div>
										  <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                        <div class="tile-stats">
					                          <div class="icon"><i class="fa fa-heart"></i>
					                          </div>
					                          <div class="count">20</div>

					                          <h3>Active Listings</h3>
					                          <p><a href="">Checkout out..</a></p>
					                        </div>
					                      </div>										  
					                      <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                        <div class="tile-stats">
					                          <div class="icon"><i class="fa fa-strikethrough"></i>
					                          </div>
					                          <div class="count">33</div>

					                          <h3>Sold Listings</h3>
					                          <p><a href="">Checkout out..</a></p>
					                        </div>
					                      </div>										  
					                      <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                        <div class="tile-stats">
					                          <div class="icon"><i class="fa fa-globe"></i>
					                          </div>
					                          <div class="count">150</div>

					                          <h3>Web Leads</h3>
					                          <p><a href="">Checkout out..</a></p>
					                        </div>
					                      </div>
					                      <div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
					                        <div class="tile-stats">
					                          <div class="icon"><i class="fa fa-street-view"></i>
					                          </div>
					                          <div class="count">168</div>

					                          <h3>Flex Leads</h3>
					                          <p><a href="">Checkout out..</a></p>
					                        </div>
					                      </div>
			                          </div>
			                          <div class="col-md-9">

										<!-- Agent More Info -->
										  <div class="agent-more-info">
					                          <div class="col-md-6">
												<div class="x_panel">
						                          <div class="x_title">
						                            <h2>Agent Information</h2>
						                            <div class="clearfix"></div>
						                          </div>
						                          <div class="x_content">
						                          	<ul>
						                          		<li>
						                          			<p>User ID: <span> 572</span></p>
						                          		</li>
						                          		<li>
						                          			<p>Agent ID: <span> 20071208001740027433000000</span></p>
						                          		</li>
						                          		<li>
						                          			<p>MLS ID: <span> 123</span></p>
						                          		</li>
						                          		<li>
						                          			<p>Spark Agent ID: <span> 3784</span></p>
						                          		</li>
						                          		<li>
						                          			<p>MLS Name: <span> ARMLS</span></p>
						                          		</li>
						                          		<li>
						                          			<p>Company Name: <span> Realty Executives East Valley</span></p>
						                          		</li>
						                          		<li>
						                          			<p>Signup Date: <span> January 28, 2017</span></p>
						                          		</li>
						                          		<li>
						                          			<p>Access Token: <span> 1sl0uikh2q2shbgnag6mbcuzm</span></p>
						                          		</li>
						                          		<li>
						                          			<p>
						                          				Token Status: 
											                    <?php if ($i % 2 == 0): ?>
											                    	<span class="label label-success">LIVE<span>
											                    <?php else: ?>
											                    	<span class="label label-danger">LOST<span>
											                    <?php endif ?></td>
						                          			</p>
						                          		</li>
						                          	</ul> 
						                          </div>
						                        </div>

												<div class="x_panel">
						                          <div class="x_title">
						                            <h2>Product Information</h2>
						                            <div class="clearfix"></div>
						                          </div>
						                          <div class="x_content">
						                          	<ul>
						                          		<li>
						                          			<p>Product: <span> Agent IDX Website</span></p>
						                          		</li>
						                          		<li>
						                          			<p>Domain Name (subdomain or custom domain): <span> <a target="_blank" href="http://propertyinaz.com/">http://propertyinaz.com/</a></p>
						                          		</li>
						                          		<li>
						                          			<p>Date of Most Recent Sold Listings: <span> January 28, 2017 </span></p>
						                          		</li>
						                          		<li>
						                          			<p>Date of Most Recent Active Listings: <span> January 28, 2017 </span></p>
						                          		</li>
						                          	</ul> 
						                          </div>
						                        </div>

												<div class="x_panel">
						                          <div class="x_title">
						                            <h2>Payment Details</h2>
						                            <div class="clearfix"></div>
						                          </div>
						                          <div class="x_content">
						                          	<ul>
						                          		<li>
						                          			<p>Paying Customer: <span> <span class="label label-success">YES<span></span></p>
						                          		</li>
						                          		<li>
						                          			<p>Last Payment Date in Stripe: <span> January 28, 2017 </span></p>
						                          		</li>
						                          	</ul> 
						                          </div>
						                        </div>
					                          </div>
					                          <div class="col-md-6">

					                          </div>
					                          <div class="col-md-6">
												<div class="x_panel">
						                          <div class="x_title">
						                            <h2>User Subscription</h2>
						                            <div class="clearfix"></div>
						                          </div>
						                          <div class="x_content">
						                          	<ul>
						                          		<li>
						                          			<p>Plan: <span> 15 Days Trial</span></p>
						                          		</li>
						                          		<li>
						                          			<p>Plan Status: <span> Active</span></p>
						                          		</li>
						                          		<li>
						                          			<p>Users Subscription: <span> Purchased</span></p>
						                          		</li>
						                          		<li>
						                          			<p>Subscribed Via: <span> Agentsquared </span></p>
						                          		</li>
						                          		<li>
						                          			<p>Join Date (Date Agent received license): <span> January 28, 2017 </span></p>
						                          		</li>
						                          	</ul> 
						                          </div>
						                        </div>
												<div class="x_panel">
						                          <div class="x_title">
						                            <h2>Cancellation Details</h2>
						                            <div class="clearfix"></div>
						                          </div>
						                          <div class="x_content">
						                          	<ul>
						                          		<li>
						                          			<p>Cancellation Date: <span> January 28, 2017 </span></p>
						                          		</li>
						                          		<li>
						                          			<p>Reason for Cancelling: <br><span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse tempore maiores aliquid, nam doloremque est, iste facere aliquam ullam harum exercitationem ducimus sed deserunt, magni officia alias blanditiis laboriosam totam.</span></p>
						                          		</li>
						                          	</ul> 
						                          </div>
						                        </div>
					                          </div>
					                          <div class="col-md-6">
												<div class="x_panel">
						                          <div class="x_title">
						                            <h2>Social Media Details</h2>
						                            <div class="clearfix"></div>
						                          </div>
						                          <div class="x_content">
						                          	<ul>
						                          		<li>
						                          			<p> Facebook Link: <span> <a target="_blank" href="https://www.facebook.com/">https://www.facebook.com/</a> </span></p>
						                          		</li>
						                          		<li>
						                          			<p> Google Business Page Link: <span> <a target="_blank" href="https://www.google.com/">https://www.google.com/</a> </span></p>
						                          		</li>
						                          		<li>
						                          			<p> LinkedIn Link: <span> <a target="_blank" href="https://www.linkedin.com/">https://www.linkedin.com/</a> </span></p>
						                          		</li>
						                          		<li>
						                          			<p> Twitter Link: <span> <a target="_blank" href="https://twitter.com/">https://twitter.com/</a> </span></p>
						                          		</li>
						                          	</ul> 
						                          </div>
						                        </div>
					                          </div>
										  </div>
										<!-- Agent More Info -->
			                          </div>
									</div>
		                        </div>
								<div class="modal-footer">
		                      		<a href="#" class="button btn-red delete-user" data-toggle="modal" data-target=".delete_<?php echo $i; ?>"> Delete </a>
		                      		<a href="#" class="button btn-blue"> Sync Listings </a>
								</div>
		                      </div>
		                    </div>
		                  </div>
	                <?php } ?>
	                </tbody>
	              </table>
	            </div>
	            <div class="pagination pull-right">
	            	<ul class="list-inline">
	            		<li class="active"><a href="#">1</a></li>
	            		<li><a href="#">2</a></li>
	            		<li><a href="#">3</a></li>
	            		<li><a href="#">4</a></li>
	            	</ul>
	            </div>
            </div>
            
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            HIVE by <a href="https://agentsquared.com/">Agentsquared</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="assets/js/main.js"></script>

  </body>
</html>