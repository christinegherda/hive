<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/default-favicon.png">
    
    <title>HIVE </title>

    <!-- Bootstrap -->
    <link href="assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/vendors/font-awesome/css/fontawesome-all.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="assets/css/main.css" rel="stylesheet">
  </head>

  <body class="grey">
    <div class="section-login-bg">
      <div class="login-wrapper">
        <form action="" class="login">
        <div class="top-bg-house"></div>
          <h3>Sign in to Hive </h3>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Email">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Password">
          </div>
          <div class="form-group">
            <a class="reset_pass" href="#">Reset password</a>
            <a href="agents.php" class="button btn-blue" type="submit">Login</a>
          </div>
          <div class="bottom-bg-house"></div>
        </form>
        <br>
        <div class="text-center">
          <p>©2018 All Rights Reserved. Agentsquared</p>
        </div>
      </div>
    </div>

  </body>
</html>