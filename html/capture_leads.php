<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/default-favicon.png">
    
    <title>HIVE </title>

    <!-- Bootstrap -->
    <link href="assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/vendors/font-awesome/css/fontawesome-all.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="assets/css/main.css" rel="stylesheet">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
          
        <?php include('common/navbar.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="col-md-12">
            <div class="page-title">
              <div class="title_left">
                <div class="col-md-12">
                  <h3>Capture Leads</h3>
                  <p>Agents who tried to purchase from <a href="https://www.agentsquared.com/pricing/">https://www.agentsquared.com/pricing/</a> </p>
                  <p>Showing 1-20 of 2000</p>
                </div>
              </div>

              <!-- Search Form -->
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-striped jambo_table table-l2">
                  <thead>
                    <tr class="headings">
                      <th class="column-title"><a href="#">No. <i class="fa fa-caret-down"></i></a> </th>
                      <th class="column-title"><a href="#">Name <i class="fa fa-caret-down"></i></a> </th>
                      <th class="column-title"><a href="#">Email  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">Phone  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">MLS  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">Product  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">Plan  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title">Action</th>
                      </th>
                      <th class="bulk-actions" colspan="7">
                        <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                  <?php for ($i=1; $i < 21; $i++) { ?>
                      <tr class="collapseable collapsed">
                        <td class=""><?php echo $i; ?></td>
                        <td class="">John Doe</td>  
                        <td class=""><a href="mailto:christine.gonx@gmail.com">johndoe@gmail.com</a></td>  
                        <td class=""><a href="tel:+123-456-789">123-456-789</a></td>  
                        <td>ARMLS</td>
                        <td>Agent IDX Website</td>
                        <td>Freemium</td>
                        <td><a href="#" class="button btn-red" data-toggle="modal" data-target=".modal-delete"> Delete</a></td>
                      </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            
          </div>
        </div>
        <!-- /page content -->
            <!-- modal delte -->
              <div class="modal fade modal_hive modal-delete" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                      </button>
                      <h4 class="modal-title" id="myModalLabel">Delete User</h4>
                    </div>
                      <form action="">
                        <div class="modal-body">
                          <p>Are you sure you want to delete John Doe?</p>
                        </div>
                        <div class="modal-footer">
                          <button class="button btn-grey" type="submit">Cancel</button>
                          <button class="button btn-red" type="submit">Delete</button>
                        </div>
                      </form>
                 </div>
               </div>
              </div>
            <!-- modal delte -->
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            HIVE by <a href="https://agentsquared.com/">Agentsquared</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="assets/js/main.js"></script>

  </body>
</html>