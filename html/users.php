<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/default-favicon.png">
	
    <title>HIVE </title>

    <!-- Bootstrap -->
    <link href="assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/vendors/font-awesome/css/fontawesome-all.min.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="assets/css/main.css" rel="stylesheet">
  </head>

  <body class="nav-sm">
    <div class="container body">
      <div class="main_container">
          
        <?php include('common/navbar.php'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="col-md-12">
            <div class="page-title">
			  <div class="title_left">
			  	  <div class="col-md-5">
	                <h3>Users <br><a href="#" data-toggle="modal" data-target=".modal-add_user" class="button btn-blue"><i class="fa fa-user-plus"></i> Add User</a></h3>
	              </div>
			  </div>

              <!-- Search Form -->
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-12">
              <div class="table-responsive">
                <table class="table table-striped jambo_table table-l2">
                  <thead>
                    <tr class="headings">
                      <th class="column-title"><a href="#">No. <i class="fa fa-caret-down"></i></a> </th>
                      <th class="column-title"><a href="#">Name <i class="fa fa-caret-down"></i></a> </th>
                      <th class="column-title"><a href="#">Email  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">Phone  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">Role  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title"><a href="#">Date Added  <i class="fa fa-caret-down"></i></a></th>
                      <th class="column-title" width="10%"><a href="#">Action  <i class="fa fa-caret-down"></i></a></th>
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                  <?php for ($i=1; $i < 6; $i++) { ?>
                      <tr class="collapseable collapsed">
                        <td class=""><?php echo $i; ?></td>
                        <td class="">John Doe</td>  
                        <td class=""><a href="mailto:christine.gonx@gmail.com">johndoe@gmail.com</a></td>  
                        <td class=""><a href="tel:+123-456-789">123-456-789</a></td>  
                        <td>Super Admin</td>
                        <td>January 28, 2017</td>
                        <td><a href="#" class="button btn-red" data-toggle="modal" data-target=".modal-delete">Delete</a></td>
                      </tr>
                  <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
		        <!-- modal delte -->
		          <div class="modal fade modal_hive modal-delete" tabindex="-1" role="dialog" aria-hidden="true">
		            <div class="modal-dialog">
		              <div class="modal-content">
		                <div class="modal-header">
		                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
		                  </button>
		                  <h4 class="modal-title" id="myModalLabel">Delete User</h4>
		                </div>
						<form action="">
			                <div class="modal-body">
								<p>Are you sure you want to delete John Doe?</p>
			                </div>
			                <div class="modal-footer">
			                	<button class="button btn-grey" type="submit">Cancel</button>
			                	<button class="button btn-red" type="submit">Delete</button>
			                </div>
						</form>
		             </div>
		           </div>
		          </div>
		        <!-- modal delte -->
	          <div class="modal fade modal_hive modal-add_user" tabindex="-1" role="dialog" aria-hidden="true">
	            <div class="modal-dialog">
	              <div class="modal-content">
	                <div class="modal-header">
	                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
	                  </button>
	                  <h4 class="modal-title" id="myModalLabel">Add User</h4>
	                </div>
					<form action="">
		                <div class="modal-body">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Email">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Phone">
							</div>
							<div class="form-group">
								<select name="" id="" class="form-control">
									<option value="">Select Role</option>
									<option value="Super Admin">Super Admin</option>
									<option value="Admin">Admin</option>
									<option value="Sales">Sales</option>
								</select>
							</div>
		                </div>
		                <div class="modal-footer">
		                	<div class="col-md-2">
		                		<button class="button btn-blue" type="submit">Submit</button>
		                	</div>
		                </div>
					</form>
	             </div>
	           </div>
	          </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            HIVE by <a href="https://agentsquared.com/">Agentsquared</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="assets/js/main.js"></script>

  </body>
</html>